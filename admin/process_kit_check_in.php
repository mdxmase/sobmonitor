<?php
session_start();
include_once('login_checker.php');
extract($_POST);


$kit_tag = $db->escape($kit_tag);
$assetTagObj = $db->query("SELECT * FROM `kits` WHERE kits.kit_asset_tag = UPPER('$kit_tag')");

if ($assetTagObj->num_rows != 0) {

    // Return the kit
    $return_obj = $db->query("UPDATE `kit_loans` INNER JOIN kits ON kits.kit_id = kit_loans.kit_id SET kit_checked_in_at = NOW(), kit_checked_in_by = $uid, kit_returned = 1 WHERE kits.kit_asset_tag = UPPER('$kit_tag')");


    $status_obj = $db->query("UPDATE `kits` SET kit_status_id = 1 WHERE kit_asset_tag = UPPER('$kit_tag')");


    if ($return_obj && $status_obj)
        echo "<div> Kit: $kit_tag Returned</div>";
    else
        echo "<div style='color: #CC0000'> Unknown Error Returning Kit: $kit_tag</div>";
}
else
    echo "<div style='color: #CC0000'>ERROR: Kit not found: $kit_tag</div>";
