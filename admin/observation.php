<?php
session_start();
include 'login_checker.php';
include 'header.php';

if(has_capabilities($uid, 'Observe SOBs')==false){
	header('Location:home.php');
	exit();
}
?>
<script>


function record_student_observe(){
	if(document.getElementById('student_id').value!=""){
		var stud = document.getElementById('student_id').value;
		$('#content').html('Please wait... Loading...').load('take_observation.php?student_id='+stud);
		$('#right_info').html('Please wait... Loading...').load('filter_sob.php?student_id='+stud,function(){
			$('.datepicker').datepicker();
			$("#keywords").autocomplete("keyword_suggestions.php");
		});
	}
}

function observed(sob_id,student_id){
	$('#sob_status_'+sob_id).html('...').load('update_sob_status.php?student_id='+student_id+'&sob_id='+sob_id);
	//$('#sob_status_'+sob_id).css('padding-top','5px');
}

function undo_observed(observed_id,sob_id){
	$('#sob_status_'+sob_id).html('...').load('update_sob_status_undo.php?observed_id='+observed_id);
}


function update_notes(sob_id,student_id){
	var notes = $('#sob_notes').val();
	
	if(document.getElementById('public').checked==true){
		var pub = 1;	
	}
	else{
		var pub = 0;
	}
	if(notes!=""){
		notes = encodeURIComponent(notes);
		$('#hidden_div').load('update_notes.php?student_id='+student_id+'&sob_id='+sob_id+'&notes='+notes+'&public='+pub,function(response){
			cover_close();
			if(pub==0){
				$('#private_notes_btn_'+sob_id).html(response);
			}
			else{
				$('#notes_btn_'+sob_id).html(response);
			}
			$.jGrowl("Comments updated successfully");
		});
	}
}

function sob_notes(sob_id,student_id){
	var height = $(window).height();
	var url = "sob_notes.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&student_id='+student_id+'&sob_id='+sob_id);
}

function sob_notes_private(sob_id,student_id){
	var height = $(window).height();
	var url = "sob_notes_private.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&student_id='+student_id+'&sob_id='+sob_id);
}

function filter_sob_observe_list(){

	
	var flag = 0;
	var levels = "";var topics = ""; var sdate = ""; var edate = ""; var sobstatus = ""; var keywords_sob_id = "";
	
	var level_arr = document.getElementsByName('levels');
	for(var i=0;i<level_arr.length;i++){
		if(level_arr[i].checked==true){
			if(levels=="")
			levels = level_arr[i].value;
			else
			levels+=","+level_arr[i].value;
			
			flag=1;
			
		}
	}
	
	var topic_arr = document.getElementsByName('topics');
	for(var i=0;i<topic_arr.length;i++){
		if(topic_arr[i].checked==true){
			if(topics=="")
			topics = topic_arr[i].value;
			else
			topics+=","+topic_arr[i].value;
			
			flag=1;
			
		}
	}
	
	
	if(document.getElementById('start_date').value!=""){
		flag=1;
		sdate = document.getElementById('start_date').value;
	}
	
	if(document.getElementById('end_date').value!=""){
		flag=1;
		edate = document.getElementById('end_date').value;
	}
	
	
	var sob_status_arr = document.getElementsByName('sob_status');
	for(var i=0;i<sob_status_arr.length;i++){
		if(sob_status_arr[i].checked==true){
			if(sobstatus=="")
			sobstatus = sob_status_arr[i].value;
			else
			sobstatus+=","+sob_status_arr[i].value;
			
			flag=1;
		}
	}
	
	var keyword_sob_arr = document.getElementsByName('search_sob');
	for(var i=0;i<keyword_sob_arr.length;i++){
			if(keywords_sob_id=="")
			keywords_sob_id = keyword_sob_arr[i].value;
			else
			keywords_sob_id+=","+keyword_sob_arr[i].value;
			
			flag=1;
	}
	
	
	if(flag==1){
		var stud = document.getElementById('filter_student_moid').value;
		$('#content').html('Please wait... Loading...').load('take_observation.php?student_id='+stud+'&levels='+levels+'&topics='+topics+'&from_date='+sdate+'&to_date='+edate+'&keywords_sob_id='+keywords_sob_id+'&sob_status='+sobstatus);
	}
	else{
		var stud = document.getElementById('filter_student_moid').value;
		$('#content').html('Please wait... Loading...').load('take_observation.php?student_id='+stud);
	}
	
	
}


function isNumberKey(evt)
{
 var charCode = (evt.which) ? evt.which : evt.keyCode

 if (charCode ==13)
	record_student_observe();

 return true;
}


function search_keywords(input,event)
{

	var keyCode=event.which? event.which : event.keyCode;

	if(parseInt(keyCode)==13)
	{
		var val = input.value;
		input.value="";
		val = encodeURIComponent(val);
		if(val!=""){
			
			$('#hidden_div').load('get_keyword_sob_id.php','keyword='+val,function(response){
					var tag_id = $(response).find('#search_keyword_id').val();
					var sob_ids = $(response).find('#key_sob_id').val();
					var html = '<div class="keyword_tags" id="tag_id_'+tag_id+'">'+val+' <input type="hidden" id="search_sob_'+tag_id+'" name="search_sob" value="'+sob_ids+'" > <a href="javascript:;" onclick="remove_search_keyword('+tag_id+')">x</a></div>'
					$('#sob_keywords_tags').append(html);
				
			});
		}
	}
}


function show_levels_sobs(level_id, stud){
	$('#right_info').html('Please wait... Loading...').load('filter_sob.php?student_id='+stud,function(){
		$('.datepicker').datepicker();
		$("#keywords").autocomplete("keyword_suggestions.php");
		document.getElementById('level_'+level_id).checked=true;
		filter_sob_observe_list();
	});
}



function remove_search_keyword(tag_id){
	$('#tag_id_'+tag_id).remove();	
}

$(window).load(function() {
	if(window.location.hash !=""){
		var hashval = window.location.hash.replace('#', '');
		if(hashval.indexOf('-')==-1){
			$('#student_id').val(hashval);
			record_student_observe();
		}
		else{
			var vals = hashval.split('-');
			$('#student_id').val(vals[0]);
			show_levels_sobs(vals[1], vals[0]);
		}
	}
});

</script>
<div id="wrapper">
    <div id="wrapper_content" style="min-width:900px;">
    	<h1 class="page_title">Observe</h1>
         <table width="100%" cellpadding="0" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Student ID :</strong> <input type="text" id="student_id" name="student_id" placeholder="Student ID" onkeypress="return isNumberKey(event)"/> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="record_student_observe()">Submit</a> </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <br>
		<br>
        <div id="content">
    	<?php
            //include 'take_observation.php';
        ?>
        </div>
        <div id="right_info"></div>
		<?php
		//include 'filter_sob.php';
	?>
    </div>
    	
</div>



<?php
include 'footer.php';
?>