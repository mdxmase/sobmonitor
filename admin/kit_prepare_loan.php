<?php
session_start();
include_once('login_checker.php');

$kit_tag = $_GET['kit_asset_tag'];
$kit_tag = $db->escape($kit_tag);
$kit_obj = $db->query("SELECT kit_type.kit_type_name, kits.kit_notes, kit_type.kit_type_default_loan_duration_length, kit_type.kit_type_default_loan_duration_unit FROM kits LEFT OUTER JOIN kit_type ON kits.kit_type_id = kit_type.kit_type_id WHERE kits.kit_asset_tag = UPPER('$kit_tag')");
$kit_no = $kit_obj->num_rows;
if($kit_no==0){
    ?>
    <table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1">
        <tr>
            <Td align="center"><b style="color:#F00">Invalid Kit Asset Tag </b></Td>
        </tr>
    </table>
    <?php

}
else {
    $kit_details = $kit_obj->row;

    ?>
    <br/>
    <br/>
    <table width="100%" cellpadding="0" cellspacing="5px">
        <tbody>
        <tr>
            <td colspan="2"><strong>Kit Asset Tag :</strong> <?php echo $kit_tag; ?></td>
        </tr>
        <br/>
        <tr>
            <td colspan="2"><strong>Kit Type :</strong> <?php echo $kit_details['kit_type_name']; ?></td>
        </tr>
        <tr>
            <td><strong>Loan Duration :</strong>
                <input type="number" id="loan_duration_length" name="loan_duration_length"
                       value="<?php echo $kit_details['kit_type_default_loan_duration_length']; ?>" placeholder="Dur."
                       style=" width:50px;">
                <select name="loan_duration_unit" id="loan_duration_unit">
                    <option
                        value="hours" <?php echo ($kit_details['kit_type_default_loan_duration_unit'] == 'hours') ? 'selected = "selected"' : ''; ?> >
                        hours
                    </option>
                    <option
                        value="days" <?php echo ($kit_details['kit_type_default_loan_duration_unit'] == 'days') ? 'selected = "selected"' : ''; ?> >
                        days
                    </option>
                    <option
                        value="weeks" <?php echo ($kit_details['kit_type_default_loan_duration_unit'] == 'weeks') ? 'selected = "selected"' : ''; ?> >
                        weeks
                    </option>
                    <option
                        value="months" <?php echo ($kit_details['kit_type_default_loan_duration_unit'] == 'months') ? 'selected = "selected"' : ''; ?> >
                        months
                    </option>
                </select>


            </td>
        </tr>
        </tbody>
    </table>

    <br/>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2"><strong>Student Number :</strong> <input type="text" id="student_number" name="student_number" placeholder="Student Number" onkeypress="return isEnterKeyStud(event)"/> &nbsp;&nbsp;
                <a class="small themebutton button" href="javascript:;" onClick="checkStudentKitLoans();">Go</a></td>
        </tr>

    </table>


    <?php
}
?>

