<?php
session_start();
include 'config.php';
include_once('db_class.php');

$currentFile = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentFile);
$page = $parts[count($parts) - 1];
 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Staff</title>
        
    <link rel="stylesheet" href="css/css-buttons.css" type="text/css" title="default" />
    <link rel="stylesheet" href="css/jquery.datepicker.css" type="text/css" title="default" />
    <link rel="stylesheet" href="css/jquery.jgrowl.css" type="text/css" title="default" />
    <link href="css/highlighted_info.css" rel="stylesheet" type="text/css" />
    <link href="css/dropdown_one.css" rel="stylesheet" type="text/css" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" />  
    <link href="css/grayout.css" rel="stylesheet" type="text/css" />  
    <script type="text/javascript" language="JavaScript" src="js/jquery.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/jquery.datepicker.js"></script>
	<script type="text/javascript" language="JavaScript" src="js/jquery.jgrowl.js"></script>
	<script type="text/javascript" language="JavaScript" src="js/jquery.autocomplete.js"></script>
    
<?php
if($page=="dashboard.php")
{
	?>
    <!--<script type="text/javascript" language="JavaScript" src="js/"></script>
   <link href="css/" rel="stylesheet" type="text/css" />-->
	<?php
}


if($page=="sobs.php" || $page=="observe_multiple.php.php" || $page=="report_sobs.php")
{
	?>
    <script type="text/javascript" language="JavaScript" src="js/sobs.js"></script>
    <?php
}


if($page=="staffs.php")
{
	?>
    <script type="text/javascript" language="JavaScript" src="js/staffs.js"></script>
    <?php
}



if($page=="students.php" or $page=="non_attending_students.php")
{
	?>
    <script type="text/javascript" language="JavaScript" src="js/students.js"></script>
    <?php
}


if($page=="topics.php")
{
	?>
    <script type="text/javascript" language="JavaScript" src="js/topics.js"></script>
    <?php
}

if($page=="manage_kit_types.php")
{
    ?>
    <script type="text/javascript" language="JavaScript" src="js/kit_types.js"></script>
    <?php
}

if($page=="kits.php")
{
    ?>
    <script type="text/javascript" language="JavaScript" src="js/kits.js"></script>
    <?php
}

?>




</head>
<body>
<div class="site_header">

<div id="logo"><img src="images/logo-mdx.gif" height="45" /></div>
<!--<div align="center" style="color:#FFFFFF;margin-top:10px;">Students Observable Behaviour</div>-->
<div style="float:right;margin:20px 10px 0px 0px;color:#FFF">Logged in as <strong><?php echo $person_loggedin; ?></strong> (<?php echo $person_loggedin_email; ?>)
</div>

<div style="clear:both"></div>
</div>

<div class="site_menu">
<ul id="menu">
	<li <?php echo ($page == "home.php")? 'class="current"':'';?>><a href="home.php"><b>Dashboard</b></a></li>
    <?php
	if(has_capabilities($uid,'Manage Staffs')==true){
    	?><li <?php echo ($page == "staffs.php")? 'class="current"':'';?>><a href="staffs.php"><b>Staff</b></a></li><?php
    }
	?>
    
   <li <?php echo ($page == "students.php" || ($page == "import_students.php" && $_GET['destination']=='sync'))? 'class="current"':'';?>><a href="javascript:;"><b>Students</b></a>
        <ul>
        	<li><a href="students.php"><b><?php if(has_capabilities($uid,'Manage Students')==true){ echo 'View / Edit';} else { echo 'View';}?></b></a></li>
		<?php
        if(has_capabilities($uid,'Manage Students')==true){
            ?>
            <li><a href="import_students.php?destination=sync"><b>Import & Sync</b></a></li>
           
            <?php
        }
        ?>
        </ul>
        </li>
    
    <?php
	if(has_capabilities($uid,'Manage SOBs')==true){
    	?><li <?php echo ($page == "topics.php")? 'class="current"':'';?>><a href="topics.php"><b>Topics</b></a></li><?php
    }
	?>
    
    <?php
	if(has_capabilities($uid,'View SOBS')==true){
    	?> <li <?php echo ($page == "sobs.php")? 'class="current"':'';?>><a href="sobs.php"><b>SOBs</b></a></li><?php
    }
	?>
    
	<?php
	if(has_capabilities($uid,'Observe SOBs')==true){
    	?><li <?php echo ($page == "observation.php" || $page == "observe_multiple.php")? 'class="current"':'';?>><a href="javascript:;" class="sub"><b>Observe</b></a>
		 <ul>
			<li><a href="observation.php"><b>Single student</b></a></li>
            <li><a href="observe_multiple.php"><b>Multiple Students</b></a></li>
		</ul></li><?php
    }
	?>
    
    <?php
	if(has_capabilities($uid,'Attendance')==true){
    	?> 	<li <?php echo ($page == "record_attendance.php" || $page == "overview_attendance.php" || $page == "check_attendace.php" || $page == "non_attending_students.php")? 'class="current"':'';?>><a href="javascript:;" class="sub"><b>Attendance</b></a>
        	<ul>
			<li><a href="record_attendance.php"><b>Record</b></a></li>
            <li><a href="overview_attendance.php"><b>Overview</b></a></li>
            <li><a href="check_attendance.php"><b>Check attendance</b></a></li>
            <li><a href="non_attending_students.php"><b>Non-attending students</b></a></li>
            <li><a href="non_registered_students.php"><b>Non-registered students</b></a></li>
            </ul>
            </li>
		<?php
    }
	?>

    
    <li <?php echo ($page == "student_report_compare.php" || $page == "report_sobs.php"  || $page == "student_report_overall.php" || ($page == "import_students.php" && $_GET['destination']=='report'))? 'class="current"':'';?>><a href="javascript:;" class="sub"><b>Reports</b></a>
    	<ul>
        	<?php /*?><li><a href="student_report.php"><b>Individual</b></a></li><?php */?>
        	<li><a href="student_report_compare.php"><b>Comparative - Each level</b></a></li>
            <li><a href="student_report_overall.php"><b>Comparative - All levels</b></a></li>
            <li><a href="report_sobs.php"><b>SOBs reporting</b></a></li>
            <li><a href="javascript:print_disengagement_report()"><b>Attendance + moodle report</b></a></li>
             <li><a href="import_students.php?destination=report"><b>Import moodle data</b></a></li>
             <li><a href="generate_attendance_xls_report.php"><b>CSV for attendance</b></a></li>
        </ul>
    </li>
    <?php /*?><li <?php echo ($page == "contact_student.php")? 'class="current"':'';?>><a href="contact_student.php"><b>Contact</b></a></li><?php */?>
    
    <?php /*
	if(has_capabilities($uid,'Manage Staffs')==true){
    	?><li <?php echo ($page == "settings.php")? 'class="current"':'';?>><a href="settings.php"><b>Settings</b></a></li><?php
    }*/
	?>

    <?php
    if(has_capabilities($uid,'Kits')==true){
        ?> 	<li <?php echo ($page == "manage_kits.php" || $page == "manage_kit_types.php" || $page == "check_kit_in.php" || $page == "check_kit_out.php")? 'class="current"':'';?>><a href="javascript:;" class="sub"><b>Kits</b></a>
            <ul>
                <li><a href="kits.php"><b>Manage Kits</b></a></li>
                <li><a href="manage_kit_types.php"><b>Manage Kit Types</b></a></li>
                <li><a href="check_out_kit.php"><b>Check-out Kits</b></a></li>
                <li><a href="check_kit_in.php"><b>Check-in Kits</b></a></li>
                <li><a href="student_kit_history.php"><b>Check Student History</b></a></li>
            </ul>
        </li>
        <?php
    }
    ?>
    
    <li><a href="logout.php"><b>Logout</b></a></li>
</ul>
</div>
