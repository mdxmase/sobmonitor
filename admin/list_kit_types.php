<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);


?>
<div id="sobs_filtered_list">
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">Type No.</th>

          <th align="left">Name</th>

          <th align="left">Description</th>

          <th align="left">Default Loan Duration</th>

          <th width="60" align="left">Action</th>
         
      </tr>
<?php
$query="select * from `kit_type` ";


$kit_type_obj = $db->query($query);
$kit_type_no = $kit_type_obj->num_rows;

if($kit_type_no!=0){
	$kit_types = $kit_type_obj->rows;
	$s=0;
	foreach($kit_types as $kit_type){
	$s++;
	?>
    <tr id="kit_type_tr_<?php echo $kit_type['kit_type_id'];?>">
          <td align="left"><?php echo $s;?></td>
          <td align="left"><?php echo $kit_type['kit_type_name'];?></Td>
          <td align="left"><?php echo $kit_type['kit_type_description'];?></Td>
          <td align="left"><?php echo $kit_type['kit_type_default_loan_duration_length'] . " " . $kit_type['kit_type_default_loan_duration_unit'];?></Td>
          <td align="left">
          <a href="javascript:;" title="Edit Topic" onClick="edit_kit_type('<?php echo $kit_type['kit_type_id'];?>')"><img src="images/edit.png" border="0" alt="Edit Kit Type" /></a>  &nbsp;&nbsp;
          <a href="javascript:;" onclick="delete_kit_type('<?php echo $kit_type['kit_type_id'];?>')" title="Delete Kit Type"><img src="images/nocapability.png" border="0" alt="Delete Kit Type" /></a>
          </td>
 
         
      </tr>
    <?php
	}
}
else{
	?>
     <tr>
        <Td align="center" colspan="7"><br /><b>-- No results found --</b></Td>
        </tr>
    <?php
}
?>
</table>
</div>
