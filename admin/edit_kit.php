<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);



$kit_obj = $db->query("SELECT * FROM `kits` WHERE `kit_id` = $kit_id");
$kit = $kit_obj->row;
	

?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">Edit Kit</th>
  </tr>
</table>
<form name="edit_kit_form" id="edit_kit_form" enctype="multipart/form-data" action="update_kit_submit.php" method="POST" target="hidden_iframe">
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<input type="hidden" id="kit_id" name="kit_id" value="<?php echo $kit_id;?>" />

<table width="100%" border="0" cellspacing="0" cellpadding="5">

    <tr>
        <td width="100"><strong>Asset Tag</strong></td>
        <td><input type="text" id="kit_asset_tag" name="kit_asset_tag" placeholder="Enter Asset Tag" value="<?php echo $kit['kit_asset_tag']; ?>" style=" width:200px; " /></td>
    </tr>

    <tr>
        <td width="100"><strong>Kit Type</strong></td>
        <td><select id="kit_type" name="kit_type" style="width:250px;">
                <option value="">Select Kit Type</option>
                <?php
                $kit_types_obj = $db->query("select * from `kit_type` where 1");
                $kit_types = $kit_types_obj->rows;

                foreach($kit_types as $kit_type){
                    ?><option <?php if ($kit_type['kit_type_id'] == $kit['kit_type_id']) {echo "selected";} ?> value='<?php echo $kit_type['kit_type_id'];?>'><?php echo $kit_type['kit_type_name'];?></option><?php
                }
                ?>
            </select>&nbsp;
        </td>
    </tr>

    <tr>
        <td><strong>Notes</strong></td>
        <td><textarea id="kit_notes" name="kit_notes" placeholder="Enter Notes" style="height:27px; width:98%;"><?php echo $kit['kit_notes']; ?></textarea></td>
    </tr>
  </tr>
</table>
<a class="small themebutton button" style="float:right;" onClick="update_kit()" href="javascript:;">Update Kit</a>


</div>
</form>