<?php
session_start();
include 'login_checker.php';
include 'header.php';
?>

<link class="include" rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
<script src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script>
$(document).bind('click', function(e) {
	var $clicked = $(e.target);
	if (!$clicked.parents().hasClass("filter_wrapper") && !$clicked.hasClass("ui-state-default") && !$clicked.hasClass("ui-icon") && !$clicked.hasClass("ui-datepicker-month") && !$clicked.hasClass("ui-datepicker-title") && !$clicked.hasClass("ui-datepicker-div") && !$clicked.hasClass("ui-datepicker") && !$clicked.hasClass("ui-datepicker-header") && !$clicked.parent().parent().parent().parent().hasClass("ui-datepicker-calendar") && !$clicked.parent().parent().parent().parent().hasClass("ui-datepicker")){
			$('.filter_holder').css('display','none');
	}
});

function open_filter(level){
		if($('#filter_holder_'+level).css('display')=='block'){
			$('#filter_holder_'+level).css('display','none');
		}
		else{
			$('#filter_holder_'+level).css('display','block');
			$('.datepicker').datepicker();
		}
}


function check_student_id_valid(){
	if(document.getElementById('student_id').value!=""){
		var stud = document.getElementById('student_id').value;
		$('#student_details').html('Please wait... Loading...').load('check_student_id_valid.php?student_id='+stud,function(){
			var status = $('#process_status').val();
			if(status=="1"){
				show_student_record_compare();
			}
			else{
				$('#content').css('display','none');
			}
		});
	}
}

function filter_graph(level){
	var flag = 0;
	var arr = document.getElementsByName('filter_topics_'+level);
	var topics = "";var sdate = ""; var edate = "";
	for(var i=0;i<arr.length;i++){
		if(arr[i].checked==true){
			if(topics=="")
			topics = arr[i].value;
			else
			topics+=","+arr[i].value;
			
			flag = 1;
		}
	}
	
	
	if(document.getElementById('start_date_from_'+level).value!=""){
		flag=1;
		sdate = document.getElementById('start_date_from_'+level).value;
		
		if(document.getElementById('start_date_to_'+level).value==""){
			$('#date_filter_status_'+level).html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
		
	}
	
	if(document.getElementById('start_date_to_'+level).value!=""){
		flag=1;
		edate = document.getElementById('start_date_to_'+level).value;
		
		if(document.getElementById('start_date_from_'+level).value==""){
			$('#date_filter_status_'+level).html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
	}
	
	if(document.getElementById('include_inactive_'+level).checked==true){
		include = '&inactive=1';
		flag=1;
	}
	else{
		include = '';
	}
	
	if(flag==1){
		var filters = '&level='+level+'&topics='+topics+'&from_date='+sdate+'&to_date='+edate+include;
	}
	else{
		var filters ="";
	}
	
	
	
		var stud = document.getElementById('student_id').value;
		var url = 'show_student_record_compare_level.php?student_id='+stud+filters;
		$.getJSON(url, function(data){
			open_filter(level);
			generate_graph(data.values, data.labels, data.colors, level, data.applied_filter, data.max_y_value)
		});

}

function generate_graph(values, labels, colors, holder, applied_filter,max_y_value){
	$.jqplot.config.enablePlugins = true;
			var s1 = values;
			var ticks = labels;

			plot1 = $.jqplot('chart'+holder, [s1], {
				// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
				animate: !$.jqplot.use_excanvas,
				seriesColors:colors,
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					shadow : false,
					rendererOptions: {
					// Set varyBarColor to tru to use the custom colors on the bars.
					varyBarColor: true
				},
					pointLabels: { show: false }
				},
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks,
						label : 'Progress of students + Expected progress (by <?php echo date('d.m.Y');?>)',
						labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					},
					yaxis:{
						tickOptions:{
						formatString: "%d",
						fontSize: '12px'
						},
					   label:'Number of SOBs',
					   max: max_y_value,
					   min: 0,
					   /*numberTicks:5,*/
					   labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					   labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					}
				},
				highlighter: { show: true }
			});
			plot1.replot();
			$('#graph_filter_info_'+holder).html(applied_filter);
}


function show_student_record_compare(){
	if(document.getElementById('student_id').value!=""){
		var stud = document.getElementById('student_id').value;
		$('#content').css('display','')

	
		var url = 'show_student_record_compare_level.php?student_id='+stud+'&level=1';
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, '1', data.applied_filter, data.max_y_value)
			$('.display_student_name').html(data.student);
		});
		
		var url = 'show_student_record_compare_level.php?student_id='+stud+'&level=2';
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, '2', data.applied_filter, data.max_y_value)
		});
		
		var url = 'show_student_record_compare_level.php?student_id='+stud+'&level=3';
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, '3', data.applied_filter, data.max_y_value)
		});
	}
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode
	
	if (charCode ==13)
	check_student_id_valid();
	
	return true;
}


</script>
<div id="wrapper">
    <div id="wrapper_content">
    	<h1 class="page_title">Comparative Report - Each level</h1>
         <table width="100%" cellpadding="0" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Student ID :</strong> <input type="text" id="student_id" name="student_id" placeholder="Student ID"  onkeypress="return isNumberKey(event)"/> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="check_student_id_valid()">Submit</a> </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <br>
        <div id="student_details"></div>
		<br>
        <div id="content" style="display:none; position:relative;">
        
        <?php $legend_graph = '<div id="graph_legend">
                <ul>
                    <li><div class="legend_key other_students"></div>Other students</li>
                    <li><div class="legend_key student_selected"></div><div class="display_student_name"></div></li>
                    <li><div class="legend_key ideal"></div>Expected by '.date('d.m.Y').'</li>
                </ul>
            </div>'; 
			
			$filter = '<div class="filter_wrapper"><b onClick="open_filter(<>)" class="highlight_color">Filter</b> 
				<div class="filter_holder highlight_color" id="filter_holder_<>"><strong>Topics</strong> <a href="javascript:;" class="close_button" onClick="open_filter(<>)" style="float:right;">X</a><br /><ul class="filter_list">';
				
		$topics_obj = $db->query("select * from `topics` where 1");
		$topics = $topics_obj->rows;
		
		foreach($topics as $topic){
			$filter.='<li><input type="checkbox" value="'.$topic['topic_id'].'" name="filter_topics_<>" id="topic_<>"> ' . $topic['topic'].'</li>';
			
		}
					
							
					$filter.='</ul><div class="clear"></div>
					
					<strong>Expected Completion Date</strong><br />
                <input type="text" style="width:80px;" placeholder="From" class="datepicker" name="start_date_from_<>" id="start_date_from_<>" onFocus="document.getElementById(\'date_filter_status_<>\').innerHTML=\'\'">&nbsp;&nbsp;
                <input type="text" style="width:80px;" placeholder="To" class="datepicker" name="start_date_to_<>" id="start_date_to_<>" onFocus="document.getElementById(\'date_filter_status_<>\').innerHTML=\'\'"><br /><span id="date_filter_status_<>" class="error"></span><br />
				<input type="checkbox" id="include_inactive_<>" name="include_inactive" value="1" /> Include inactive students<br />
				<a class="small themebutton button" href="javascript:;" onClick="filter_graph(\'<>\')">Submit</a>
				</div>
			</div>';
			
			$filter_1 = str_replace('<>','1',$filter);
			$filter_2 = str_replace('<>','2',$filter);
			$filter_3 = str_replace('<>','3',$filter);
			?>
            
            
    		<!--<div class="canvas_holder" style="display:none;"><div><h2>All Levels</h2></div><canvas id="canvas0" height="450" width="600"></canvas></div>-->
            
            
            <div class="canvas_holder">
            	<div class="graph_display_details"><h2>Threshold <?php echo $filter_1;?></h2>
                    <div id="graph_filter_info_1" class="graph_filter_info"><strong>Topics</strong> : All <br /><strong> Expected Completion Date</strong> : Any</div>
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart1" style=" width:100%; height:450px;"></div>
            </div>
            
            <div class="clear"></div>
            <br />
			<br />
            <div class="canvas_holder">
            	<div class="graph_display_details"><h2>Typical <?php echo $filter_2;?></h2>
                    <div id="graph_filter_info_2" class="graph_filter_info"><strong>Topics</strong> : All <br /><strong> Expected Completion Date</strong> : Any</div>
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart2" style=" width:100%; height:450px;"></div>
            </div>
            <div class="clear"></div>
            <br />
			<br />
            
            <div class="canvas_holder">
            	<div class="graph_display_details"><h2>Excellent <?php echo $filter_3;?></h2>
                    <div id="graph_filter_info_3" class="graph_filter_info"><strong>Topics</strong> : All <br /><strong> Expected Completion Date</strong> : Any</div>
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart3" style=" width:100%; height:450px;"></div>
            </div>
        </div>
        
    </div>
    	
</div>



<?php
include 'footer.php';
?>