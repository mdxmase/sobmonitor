<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');

// SELECT * FROM STUDENTS WHERE status=0;
// select a.week, max(w.week_start), max(s.student_id), max(s.staff_id), 
// max(s.network_name), max(s.firstname), max(s.lastname), max(s.email), 
// s.student_number, max(s.student_status), max(s.visa), max(s.foundation), 
// count(*) as sessions_attended from week as w , students as s left outer 
// join attendance as a ON s.student_number=a.studid group by a.week, 
// s.student_number having max(s.student_status) = 0
//
// select w.week_id, s.student_number, count(*) as sessions_attended from 
// week as w , students as s left outer join attendance as a ON 
// s.student_number=a.studid group by w.week_id, s.student_number having 
// max(s.student_status) = 0 
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=overall_weekly_attendance_CSD1000.csv");

if(has_capabilities($uid,'Manage Students')==true){

  $tutors = array(); 
  $tutor_obj = $db->query("select * from `staffs`");
  $tutors_rows = $tutor_obj->rows;

  foreach($tutors_rows as $tut) {
    $tutors[$tut['staff_id']] = $tut['firstname']." ".$tut['lastname'];
  }
  $query =  "
    select w.week_id, s.student_number, count(a.attendance_id) as sessions_attended,
      max(w.week_start) as weekstart,  max(s.staff_id) as staffid,
      max(s.network_name) as networkname, max(s.firstname) as stfirstname, 
      max(s.lastname) as stlastname, max(s.email) as stemail,
      max(s.student_status), max(s.visa) as stvisa, max(s.foundation) as stfoundation
    from 
    (students as s, week as w)  
      left outer join attendance as a ON 
      w.week_id = a.week and s.student_number=a.studid 
      group by w.week_id, s.student_number 
      having max(s.student_status) = 0  
  ORDER BY s.student_number,w.week_id, sessions_attended ASC";
  $student_obj = $db->query($query);
 
  $the_whole_lot = array();
  $students = $student_obj->rows;
  array_push($the_whole_lot,array(
    "Week number","Scheduled sessions","Student Number","Sessions attended",
    "Week starting","Tutor","Network Name","First Name","Last Name","Email",
    "Visa?","Foundation?"));
  foreach($students as $student){
    array_push($the_whole_lot, 
      array($student['week_id'],6,
      $student['student_number'],
      $student['sessions_attended'],
      $student['weekstart'],
      $tutors[$student['staffid']],
      $student['networkname'],$student['stfirstname'],$student['stlastname'],
      $student['stemail'],$student['stvisa'],$student['stfoundation']));
  }
  $output = fopen("php://output", "w");
  foreach ($the_whole_lot as $row) {
    fputcsv($output, $row); // here you can change delimiter/enclosure
  }
  fclose($output);
}

?>
