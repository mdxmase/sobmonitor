<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);


?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">Kit History</th>
  </tr>
</table>
<div style="height:<?php echo $height-200;?>px; overflow-y:scroll; display: block; padding:10px;">
    <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
        <tr class="table_heading">
            <th align="left">Name</th>
            <th align="left">Student Number</th>
            <th align="left">Check Out</th>
            <th align="left">Check In</th>
            <th align="left">Due Date</th>

        </tr>

        <?php
        $query="SELECT students.firstname, students.lastname, students.student_number, kit_loans.kit_checked_out_at, kit_loans.kit_checked_in_at, kit_loans.kit_due_at FROM kit_loans LEFT OUTER JOIN students ON kit_loans.student_id = students.student_id LEFT OUTER JOIN kits ON kit_loans.kit_id = kits.kit_id LEFT OUTER JOIN kit_status ON kits.kit_status_id = kit_status.kit_status_id WHERE kit_loans.kit_id = $kit_id ORDER BY kit_loans.kit_checked_out_at DESC";

        $loans_obj = $db->query($query);
        $loans_no = $loans_obj->num_rows;

        if($loans_no!=0){
            $loans = $loans_obj->rows;
            $s=0;
            foreach($loans as $loan){
                ?>
                <tr>
                    <td align="left" valign="top"><?php echo $loan['firstname'] . " " . $loan['lastname'];?></td>
                    <td align="left" valign="top"><?php echo $loan['student_number'];?></td>
                    <td align="left" valign="top"><?php echo date("d-m-Y H:i", strtotime($loan['kit_checked_out_at']));?></td>
                    <td align="left" valign="top"><?php echo (is_null($loan['kit_checked_in_at']) ? '' :date("d-m-Y H:i", strtotime($loan['kit_checked_in_at'])));?></td>
                    <td valign="top" align="left"><?php echo date("d-m-Y H:i", strtotime($loan['kit_due_at']));?></td>


                </tr>
                <?php
            }
        }
        else{
            ?>
            <tr>
                <Td align="center" colspan="7"><br /><b>-- No Loans Found --</b></Td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>