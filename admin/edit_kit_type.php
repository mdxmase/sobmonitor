<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);


$kit_typeid = $kit_type_id;
$kit_type_obj = $db->query("SELECT * FROM `kit_type` WHERE `kit_type_id` = '$kit_typeid'");
$kit_type = $kit_type_obj->row;
	

?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">EDIT KIT TYPE</th>
  </tr>
</table>
<div style="max-height:300px; overflow:auto; padding:10px;">
<form name="sob_form" id="sob_form" enctype="multipart/form-data" action="kit_type_update.php" method="POST" target="hidden_iframe">
<input name="kit_type_id" id="kit_type_id" type="hidden" value="<?php echo $kit_typeid ;?>">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <tr>
        <td><strong>Name</strong></td>
        <td height="50"><input type="text" id="kit_type_name" name="kit_type_name" value="<?php echo $kit_type['kit_type_name'] ;?>" placeholder="Enter Kit Type Name" style=" width:200px;">

        </td>
    </tr>
    <tr>
        <td><strong>Description</strong></td>
        <td height="50"><input type="text" id="kit_type_description" name="kit_type_description" value="<?php echo $kit_type['kit_type_description'] ;?>" placeholder="Enter Kit Description ... What does it contain?" style=" width:500px;">

        </td>
    </tr>

    <tr>
        <td><strong>Default Loan Duration</strong></td>
        <td height="50"><input type="number" id="kit_type_default_loan_duration_length" name="kit_type_default_loan_duration_length" value="<?php echo $kit_type['kit_type_default_loan_duration_length'] ;?>" placeholder="Dur." style=" width:50px;">
            <select name="kit_type_default_loan_duration_unit" id="kit_type_default_loan_duration_unit">
                <option value="hours" <?php echo ($kit_type['kit_type_default_loan_duration_unit'] == 'hours')? 'selected = "selected"':'';?> >hours</option>
                <option value="days" <?php echo ($kit_type['kit_type_default_loan_duration_unit'] == 'days')? 'selected = "selected"':'';?> >days</option>
                <option value="weeks" <?php echo ($kit_type['kit_type_default_loan_duration_unit'] == 'weeks')? 'selected = "selected"':'';?> >weeks</option>
                <option value="months" <?php echo ($kit_type['kit_type_default_loan_duration_unit'] == 'months')? 'selected = "selected"':'';?> >months</option>
            </select>


        </td>
    </tr>
  </tr>

  
</table>
<a class="small themebutton button" style="float:right;" onClick="update_kit_type()" href="javascript:;">Edit Kit Type</a>

</form>
</div>