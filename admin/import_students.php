<?php
session_start();
include 'login_checker.php';
include 'header.php';
 

?>
<script>
function upload_student(destination){
	
		if(destination == 'sync')
		document.add_students.action="sync_students.php";
		
		else if(destination == 'report')
		document.add_students.action="upload_moodle_log.php";
		
		document.add_students.submit();
		$('#submit_button').text('Uploading..');
		$('#submit_button').attr('href','javascript:');
		document.getElementById('base_d').innerHTML='<br><div id="loading_div" align="center" style="width:100%;">Please wait..uploading...</div>';
}


</script>
<div id="wrapper">
    <div id="wrapper_content">
    
    <h1 class="page_title"><?php if($_GET['destination'] == 'sync') echo 'Import Students'; else if($_GET['destination'] == 'report') echo 'Import Moodle generated data';?></h1>
        <br>

      <div id="page_contents">
      <?php if($_GET['destination'] == 'sync') { ?>
      Click <a href="sample_excel/sample_student_list.xls" >here</a> to download a sample file<br><br>
      <?php } else if($_GET['destination'] == 'report') {?>
      Click <a href="sample_excel/sample_moodle_list.xls" >here</a> to download a sample file<br><br>
      <?php } ?>
      
      
       <form id="add_students" name="add_students"  enctype="multipart/form-data" method="post" target="gframe" >
       Upload an excel file : <input type="file" name="student_upload" id="student_upload"><br><br>
       <a href="javascript:;" id="submit_button" onClick="upload_student('<?php echo $_GET['destination']?>')" class="small themebutton button"><span>Submit</span></a> 
       </form>
       
         <iframe name="gframe" id="gframe" style="display:none;"></iframe>
         <br>
         <div id="base_d" name="base_d"></div>
      </div>
      
  </div>
</div>
<?php
include 'footer.php';
?>