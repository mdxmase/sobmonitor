<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);


?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">Add Kit</th>
  </tr>
</table>
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<form name="kit_form" id="kit_form" enctype="multipart/form-data" action="kit_submit.php" method="POST" target="hidden_iframe">

<table width="100%" border="0" cellspacing="0" cellpadding="5">

  <tr>
    <td width="100"><strong>Asset Tag</strong></td>
    <td><input type="text" id="kit_asset_tag" name="kit_asset_tag" placeholder="Enter Asset Tag" style=" width:200px; " /></td>
  </tr>

<tr>
    <td width="100"><strong>Kit Type</strong></td>
    <td><select id="kit_type" name="kit_type" style="width:250px;">
    	<option value="">Select Kit Type</option>
        <?php
		$kit_types_obj = $db->query("select * from `kit_type` where 1");
        $kit_types = $kit_types_obj->rows;
		
		foreach($kit_types as $kit_type){
			?><option value='<?php echo $kit_type['kit_type_id'];?>'><?php echo $kit_type['kit_type_name'];?></option><?php
		}
		?>
    </select>&nbsp;
    </td>
  </tr>

    <tr>
        <td><strong>Notes</strong></td>
        <td><textarea id="kit_notes" name="kit_notes" placeholder="Enter Notes" style="height:27px; width:98%;"></textarea></td>
    </tr>
</table>
<a class="small themebutton button" style="float:right;" onClick="add_kit()" href="javascript:;">Add Kit</a>

</form>
</div>