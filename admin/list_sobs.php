<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);



?>
<div id="sobs_filtered_list">
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="70" align="left">SOB ID<span class="sort_field" style="float:right;" id="sort_field_sobid"><a id="button_level" href="javascript:sorting_field_name('sob_id')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th width="110" align="left">Level <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('level')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th width="120" align="left">Topic <span class="sort_field" style="float:right;" id="sort_field_topic"><a id="button_topic" href="javascript:sorting_field_name('topic')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th align="left">SOB</th>
          <th width="90" align="left">Start Date <span class="sort_field" style="float:right;" id="sort_field_expected_start_date"><a id="button_expected_start_date" href="javascript:sorting_field_name('expected_start_date')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th width="110" align="left">Expected Completion Date<span class="sort_field" style="float:right;" id="sort_field_expected_completion_date"><a id="button_expected_completion_date" href="javascript:sorting_field_name('expected_completion_date')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <?php
          if(has_capabilities($uid, 'Manage SOBs')==true){
		  ?>
          <th width="80" align="left">Edit</th>
          <?php
		  }
		  ?>
      </tr>
<?php
$query="select s.sob_id, s.sob, s.url, l.level, t.topic, s.sob_notes, s.expected_completion_date , s.expected_start_date from `sobs` s, `levels` l, `topics` t where s.level_id = l.level_id and s.topic_id = t.topic_id ";

if($start_date_from !='' && $start_date_to =='' )
	{
		$start_date_from=date_mysql($start_date_from);
		$query.=" and s.expected_start_date >= '$start_date_from' ";
	}
if($start_date_from =='' && $start_date_to!='')
{
	$start_date_to=date_mysql($start_date_to);
	$query.=" and s.expected_start_date <= '$start_date_to '";
}
if($start_date_from!="" && $start_date_to!="")
{
	$start_date_from=date_mysql($start_date_from);
	$start_date_to=date_mysql($start_date_to);
	$query.="and s.expected_start_date BETWEEN '$start_date_from' and '$start_date_to' ";	
}
if($completed_date_from!='' && $completed_date_to=='' )
{
		$completed_date_from=date_mysql($completed_date_from);
		$query.=" and s.expected_completion_date >= '$completed_date_from' ";
}
if($completed_date_to!='' && $completed_date_from=='' )
{
	$completed_date_to=date_mysql($completed_date_to);
	$query.=" and s.expected_completion_date <= '$completed_date_to'";
}
if($completed_date_from!="" && $completed_date_to!="")
{
	$completed_date_to=date_mysql($completed_date_to);
	$completed_date_from=date_mysql($completed_date_from);
	$query.=" and s.expected_completion_date BETWEEN '$completed_date_from' and '$completed_date_to' ";	
}
if($sob_filter!="")
{
	$query .= " and s.sob like '%$sob_filter%' ";
}
if(isset($filter_topics))
{
	 $filter_topics= implode(',',$filter_topics);
	 $query .= " and s.topic_id in ( $filter_topics ) ";
}
if(isset($filter_levels))
{
	 $filter_levels= implode(',',$filter_levels);
	 $query .= " and s.level_id in ( $filter_levels ) ";
}

if(isset($filter_keywords)){
	$keywords_sob_id = implode(',',$filter_keywords);
	
	if($keywords_sob_id!=""){
			$query.=" AND s.sob_id IN ($keywords_sob_id) ";
	}
}


if($sorting_field_name!=""){
	$query.=" ORDER BY $sorting_field_name $sorting_by";
}
else{
	$query.=" ORDER BY s.sob_id";
}


$sobs_obj = $db->query($query);
$sobs_no = $sobs_obj->num_rows;

if($sobs_no!=0){
	$sobs = $sobs_obj->rows;
	$s=0;
	foreach($sobs as $sob){
	$s++;
	?>
    <tr id="sob_tr_<?php echo $sob['sob_id'];?>">
          <td align="left" valign="top"><?php echo $sob['sob_id'];?></td>
          <td align="left" valign="top"><?php echo $sob['level'];?></Td>
          <td valign="top" align="left"><?php echo $sob['topic'];?></Td>
          <td align="left" valign="top"><?php echo $sob['sob'];?>
          <?php
		 $sob_id_keyword= $sob['sob_id'];
		 $sob_keywords="";
		  $keywords_obj = $db->query("SELECT t.tag_id, k.keyword FROM `keywords` k, `keywords_sobs` t WHERE t.keyword_id = k.keyword_id and t.sob_id = '$sob_id_keyword'");
		$key_no = $keywords_obj->num_rows;
		if($key_no>0){
			$keywords = $keywords_obj->rows;
			
			foreach($keywords as $keyword){
				$sob_keywords.=$keyword['keyword']." | ";
			}
		}
		$sob_keywords= substr($sob_keywords, 0, -2);
		
		  ?>
          <span class="keyword_class"><?php if($sob_keywords!="") { ?><br /><strong>Keywords : </strong><?php }echo $sob_keywords; ?></span>

          <span class="keyword_class"><?php if($sob['url']!="") { ?><br /><strong>URL : </strong><?php }echo $sob['url']; ?></span>
          
          <span class="keyword_class"><?php if($sob['sob_notes']!="") { ?><br /><strong>Notes : </strong><?php }echo $sob['sob_notes']; ?></span>
			</Td>
          <td valign="top"><?php echo date_ft($sob['expected_start_date']);?></Td>
          <td valign="top"><?php echo date_ft($sob['expected_completion_date']);?></Td>
          
          <?php
          if(has_capabilities($uid, 'Manage SOBs')==true){
		  ?>
          <td align="left" valign="top">
          <a href="javascript:;" onclick="edit_sob('<?php echo $sob['sob_id'];?>')" title="Edit SOB"><img src="images/edit.png" border="0" alt="Edit SOB" /></a> &nbsp;&nbsp;
          <a href="javascript:;" onclick="delete_sob('<?php echo $sob['sob_id'];?>')" title="Delete SOB"><img src="images/nocapability.png" border="0" alt="Delete SOB" /></a> &nbsp;&nbsp;
          <a href="javascript:;" onclick="discussion_sob('<?php echo $sob['sob_id'];?>')" title="SOB Notes"><img src="images/notes.png" border="0" alt="SOB Notes" /></a>
          </td>
          <?php
		  }
		  ?>
         
      </tr>
    <?php
	}
}
else{
	?>
     <tr>
        <Td align="center" colspan="7"><br /><b>-- No results found --</b></Td>
        </tr>
    <?php
}
?>
</table>
</div>
<?php /*
if($sob_filter!="")
{
?>
<script>
var src_str = $("#sobs_filtered_list").html();
var term = "<?php echo $sob_filter?>";
term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
var pattern = new RegExp("("+term+")", "i");

src_str = src_str.replace(pattern, "<mark>$1</mark>");
src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

$("#sobs_filtered_list").html(src_str);

$('#sobs_filtered_list').text().highlight('<?php echo $sob_filter?>');

</script>
<?php
}
*/?>