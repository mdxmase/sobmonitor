<?php
session_start();
include 'login_checker.php';
include 'header.php';

if(has_capabilities($uid, 'Kits')==false){
	header('Location:home.php');
	exit();
}
?>
<script>

function isNumberKey(evt)
{
 var charCode = (evt.which) ? evt.which : evt.keyCode

 if (charCode ==13)
	show_history();

 return true;
}




function show_history(){
    if(document.getElementById('student_id').value!="") {
        var stud = document.getElementById('student_id').value;
        stud = stud.toUpperCase();
        $('#content').html('Please wait... Loading...').load('list_student_kits.php?student_id=' + stud,);
    }
}





</script>
<div id="wrapper">
    <div id="wrapper_content" style="min-width:900px;">
    	<h1 class="page_title">Check Student Kit History</h1>
         <table width="100%" cellpadding="0" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Student ID :</strong> <input type="text" id="student_id" name="student_id" placeholder="Student ID" onkeypress="return isNumberKey(event)"/> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="record_student_observe()">Submit</a> </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <br>
		<br>
        <div id="content">
    	<?php
            //include 'take_observation.php';
        ?>
        </div>
		<?php
		//include 'filter_sob.php';
	?>
    </div>
    	
</div>



<?php
include 'footer.php';
?>