<?php
session_start();
include_once('login_checker.php');
extract($_POST);


$duration_length = $db->escape($duration_length);
$duration_unit = $db->escape($duration_unit);
$student_number = $db->escape($student_number);
$kit_tag = $db->escape($kit_tag);

// Return the kit if it wasn't returned
$db->query("UPDATE `kit_loans` INNER JOIN kits ON kits.kit_id = kit_loans.kit_id SET kit_checked_in_at = NOW(), kit_checked_in_by = $uid, kit_returned = 1 WHERE kits.kit_asset_tag = UPPER('$kit_tag')");

// Create a loan row
$due_date = date("Y/m/d G:i:s", strtotime("+$duration_length $duration_unit"));

$db->query("UPDATE `kits` SET kit_status_id = 2 WHERE kit_asset_tag = UPPER('$kit_tag')");

$create_loan = $db->query("INSERT INTO `kit_loans` (kit_loans.student_id, kit_loans.kit_id, kit_loans.kit_due_at, kit_loans.kit_checked_out_at, kit_loans.kit_checked_out_by, kit_loans.kit_returned) SELECT student_id, (SELECT kit_id FROM kits WHERE kit_asset_tag = UPPER('$kit_tag')), '$due_date', NOW(), $uid, 0 FROM students WHERE student_number = '$student_number'");

if ($create_loan) {
    echo "success";
} else {
//    echo "error";
}
