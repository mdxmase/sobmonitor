// JavaScript Document



function add_kit(){
	var flag=0;
	
	if(document.getElementById('kit_asset_tag').value==""){
		document.getElementById('kit_asset_tag').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('kit_asset_tag').style.borderColor='';
	}
	
	if(document.getElementById('kit_type').value==""){
		document.getElementById('kit_type').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('kit_type').style.borderColor='';
	}

	
	if(flag==0){
		document.kit_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}

function update_kit(){
	var flag=0;
	
	if(document.getElementById('kit_asset_tag').value==""){
		document.getElementById('kit_asset_tag').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('kit_asset_tag').style.borderColor='';
	}
	
	if(document.getElementById('kit_type').value==""){
		document.getElementById('kit_type').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('kit_type').style.borderColor='';
	}

	
	if(flag==0){
		document.edit_kit_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}
function after_update_kit_submit(){
	cover_close();
	$.jGrowl("Kit updated successfully");
	/*$('#page_contents').html('Loading... Please wait...').load('list_sobs.php');*/
	search_kit();
}

function after_kit_submit(){
	$.jGrowl("Kit added successfully");
	$('#page_contents').html('Loading... Please wait...').load('list_kits.php');
}



function edit_kit(kit_id){
	var height = $(window).height();
	var url = "edit_kit.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&kit_id='+kit_id,function(){});
}


function add_new_kit(){
	var height = $(window).height();
	var url = "add_new_kit.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height,function(){
	});
}

function delete_kit(kit_id){
	var conf = confirm("Are you sure that you want to delete this kit?");
	if(conf==true){
		$.post('delete_kit.php',{kit_id:kit_id}, function(response){
			$('#hidden_div').html(response);
		})
	}
}

function after_kit_delete(kit_id){
	$('#sob_tr_'+kit_id).remove();
	$.jGrowl("Kit has been deleted");
}

function search_kit()  {
	$.post('update_overdue_kits.php');
	$('#page_contents').load('list_kits.php',$('#kit_filter_form').serialize(),function(response){
																						
	});
}

function kit_history(kit_id){
    var height = $(window).height();
    var url = "kit_history.php";
    grayOut(true,'grayOut_center_div',800);
    $('#grayOut_center_div').load(url,'height='+height+'&kit_id='+kit_id,function(){
    });
}
