

function delete_kit_type(kit_type_id){
	var conf = confirm("Are you sure that you want to delete this kit type?");
	if(conf==true){
		$.post('delete_kit_type.php',{kit_type_id:kit_type_id}, function(response){
			$('#hidden_div').html(response);
		})
	}
}

function after_kit_type_delete(kit_type_id){
	$('#kit_type_tr_'+kit_type_id).remove();
	$.jGrowl("Kit type has been deleted");
}


function add_new_kit_type(){
	var height = $(window).height();
	var url = "add_new_kit_type.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}


function edit_kit_type(id){
	var height = $(window).height();
	var url = "edit_kit_type.php?kit_type_id="+id;
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}

function add_kit_type(){
	var flag=0;
	
	
	if(document.getElementById('kit_type_name').value==""){
		document.getElementById('kit_type_name').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('kit_type_name').style.borderColor='';
	}

    if(document.getElementById('kit_type_description').value==""){
        document.getElementById('kit_type_description').style.borderColor='#FF0000';
        flag=1;
    }
    else{
        document.getElementById('kit_type_description').style.borderColor='';
    }

    if(document.getElementById('kit_type_default_loan_duration_length').value==""){
        document.getElementById('kit_type_default_loan_duration_length').style.borderColor='#FF0000';
        flag=1;
    }
    else{
        document.getElementById('kit_type_default_loan_duration_length').style.borderColor='';
    }
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}

function update_kit_type(){
	var flag=0;
	
	
	if(document.getElementById('kit_type_name').value==""){
		document.getElementById('kit_type_name').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('kit_type_name').style.borderColor='';
	}

    if(document.getElementById('kit_type_description').value==""){
        document.getElementById('kit_type_description').style.borderColor='#FF0000';
        flag=1;
    }
    else{
        document.getElementById('kit_type_description').style.borderColor='';
    }

    if(document.getElementById('kit_type_default_loan_duration_length').value==""){
        document.getElementById('kit_type_default_loan_duration_length').style.borderColor='#FF0000';
        flag=1;
    }
    else{
        document.getElementById('kit_type_default_loan_duration_length').style.borderColor='';
    }
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}
function after_kit_type_submit(){
	$.jGrowl("Kit type added successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_kit_types.php');
}


function after_kit_type_update(){
	$.jGrowl("Kit type updated successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_kit_types.php');
}
