

function delete_topic(topic_id){
	var conf = confirm("Are you sure that you want to delete this topic?");
	if(conf==true){
		$.post('delete_topic.php',{topic_id:topic_id}, function(response){
			$('#hidden_div').html(response);
		})
	}
}

function after_topic_delete(topic_id){
	$('#topic_tr_'+topic_id).remove();
	$.jGrowl("Topic has been deleted");
}


function add_new_topic(){
	var height = $(window).height();
	var url = "add_new_topic.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}


function edit_topic(id){
	var height = $(window).height();
	var url = "edit_topic.php?topic_id="+id;
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}

function add_topic(){
	var flag=0;
	
	
	if(document.getElementById('topic').value==""){
		document.getElementById('topic').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('topic').style.borderColor='';
	}
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}

function update_topic(){
	var flag=0;
	
	
	if(document.getElementById('topic').value==""){
		document.getElementById('topic').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('topic').style.borderColor='';
	}
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}
function after_topic_submit(){
	$.jGrowl("Topic added successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_topics.php');
}


function after_topic_update(){
	$.jGrowl("Topic updated successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_topics.php');
}
