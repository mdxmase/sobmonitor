<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);
//sob_observations

$topics_obj = $db->query("SELECT * FROM `contact_thread` WHERE `thread_id` = '$thread_id'");
$topics_detail = $topics_obj->row;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">VIEW THREAD</th>
  </tr>
</table>
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<h3><?php echo $topics_detail['thread_subject'];?></h3>
<p><?php echo $topics_detail['thread_message'];?></p>

<table width="100%" border="0" cellspacing="0" cellpadding="5" class="dues_table" id="reply_table">
    <tr class="dues_header_tr">
        <th width="120">Replied by</th>
        <th>Reply message</th>
        <th width="100">Datetime</th>
    </tr>
    <?php
    
	$thread_obj = $db->query("SELECT * FROM `contact_thread_replies` WHERE `thread_id` = '$thread_id'");
	$reply_no = $thread_obj->num_rows; 
	if($reply_no>0){
		$replies = $thread_obj->rows;
		$r=0;
		foreach($replies as $reply){
			$r++;
			
			?>
			<tr>
				<td><?php echo ($reply['replied_by']=="1")? 'You':'Student';?></td>
				<td><?php echo $reply['replied_message'];?></td>
				<td><?php echo date('d.m.Y',strtotime($reply['replied_datetime']));?></td>
			</tr>
			<?php
		}
	}
	else{
		echo '<tr><td colspan="3">No reply found</td></tr>';
	}
    ?>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    <td>
    <form name="reply_form" id="reply_form" enctype="multipart/form-data" method="POST" target="hidden_iframe">
    <input name="thread_id" id="thread_id" type="hidden" value="<?php echo $thread_id;?>">
    <textarea id="thread_reply" name="thread_reply" placeholder="Reply" style="height:50px; width:98%;"></textarea>
    </form>
    </td>
    <td width="100"><a class="small themebutton button" style="float:right;" onClick="insert_reply()" href="javascript:;">Submit</a></td>
    </tr>
</table>
</div>