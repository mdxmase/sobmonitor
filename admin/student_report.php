<?php
session_start();
include 'login_checker.php';
include 'header.php';
?>
<script>

function show_student_record(){
	if(document.getElementById('student_id').value!=""){
		var stud = document.getElementById('student_id').value;
		$('#content').html('Please wait... Loading...').load('show_student_record.php?student_id='+stud);
	}
}

function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : evt.keyCode
		
         if (charCode ==13)
			show_student_record();
   
         return true;
      }



</script>
<div id="wrapper">
    <div id="wrapper_content">
    	
         <table width="100%" cellpadding="0" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Student ID :</strong> <input type="text" id="student_id" name="student_id" placeholder="Student ID"  onkeypress="return isNumberKey(event)"/> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="show_student_record()">Submit</a> </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <br>
		<br>
        <div id="content">
    	
        </div>
        
    </div>
    	
</div>



<?php
include 'footer.php';
?>