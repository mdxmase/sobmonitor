<?php
session_start();
include 'login_checker.php';
include 'header.php';


if(has_capabilities($uid, 'Kits')==false){
	header('Location:home.php');
	exit();
}


?>
<script>
$(document).ready(function() {
	$('.datepicker').datepicker();
});


function filter_kit()
{
	$('#page_contents').load('list_kits.php');

}
</script>
<div id="wrapper">
    <div id="wrapper_content">
    <h1 class="page_title">Manage Kits</h1>
<br />
<br />

      <form id="kit_filter_form" name="kit_filter_form" >
        <div class="sob_filter_holder highlight_color">
        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td class="sub_headings" align="left">Filter Kits</td>
            <td align="right" width="100"><a href="javascript:;" class="close_button" onclick="toggle_graph('kit_filter',this)">- Hide</a></td>
        </tr>
        </table>
        <div  id="kit_filter">
        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td><strong>Kit Types</strong></td>
            <td colspan="2"> <?php
					$kit_type_obj = $db->query("select * from `kit_type`");
					$kit_types = $kit_type_obj->rows;
					$l=0;
					foreach($kit_types as $kit_type){
						?>
						<input type="checkbox" id="kit_type_<?php echo $kit_type['kit_type_id'];?>" name="filter_kit_types[<?php echo $l++;?>]" value="<?php echo $kit_type['kit_type_id'];?>"> <?php echo $kit_type['kit_type_name'];?>
						<?php
					}
		?></td>
         </tr>
         <tr>
            <td><strong>Status</strong></td>
            <td colspan="2">
            
             <?php
				$kit_status_obj = $db->query("select * from `kit_status`");
				$kit_statuss = $kit_status_obj->rows;
				$t=0;
				foreach($kit_statuss as $kit_status){
					?>
					<input type="checkbox" id="kit_status_<?php echo $topic['kit_status_id'];?>" name="filter_kit_status[<?php echo $t++;?>]" value="<?php echo $kit_status['kit_status_id'];?>"> <?php echo $kit_status['kit_status_name'];?>
					<?php
				}
		?>
            </td>
          </tr>

            <tr>
                <td><strong>Has Note?</strong></td>
                <td colspan="2">
                    <input type="checkbox" id="kit_note" name="filter_kit_note" value="true">
                </td>
            </tr>






          <tr>
            <td style="border-top:none; border-right:none"><span style="border-top:none"><a class="small themebutton button" onclick="search_kit()" href="javascript:;">Filter</a></span></td>

        </tr>
        </table>
        </div>
        </div>
        <br />

        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td class="sub_headings" align="left">List of Kits</td>
            <td align="right">
			<?php
            if(has_capabilities($uid, 'Kits')==true){
				?>
				<a class="small themebutton button" style="float:right;" onClick="add_new_kit()" href="javascript:;">Add Kit</a>
				<?php
            }
            ?>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        </table>
        </form>


        <div id="page_contents">
            <?php
            include 'list_kits.php';
            ?>
      </div>
      
  </div>
</div>
<?php
include 'footer.php';
?>