<?php
session_start();
include_once 'login_checker.php';
include 'header.php';
extract($_GET);
?>

<style>
    #myDiv {
        width: 90%;
        margin: 0 auto;
    }

    #myDiv div.individual_student {
        padding: 5px;
        margin: 5px;
        height: 25px;
        line-height: 25px;
    }

    #myDiv div.individual_student:nth-child(2) {
        background: #EAEAEA;
    }

    .selected_stud_option {
        float: right;
        height: 25px;
        line-height: 25px;
        margin: 0 !important;
        text-align: right;
        width: 500px;
    }

    .selected_stud_option img {
        position: relative;
        top: 3px;
    }

</style>


<?php
/*
   This is copied from attendance monitoring, version of 1st Oct 2013.
 */

// We should simplify and only show now running CRN for this person,
// all CRNs for first year, and possibility to choose a week (default to
// curweek).

// The page should reload after selecting the CRN to get the list of students


// Just a variable to see if we have something to do.
$firsttime = false;
// If we receive a crn, this is where it will be
$chosencrn = "";

if (isset ($_POST['nowcrn'])) {
    $nowcrn = $_POST['nowcrn'];
    $chosencrn = $nowcrn;
} else if (isset($_GET['nowcrn'])) {
    $nowcrn = $_GET['nowcrn'];
    $chosencrn = $nowcrn;
} else if (isset($_POST['crn'])) {
    $crn = $_POST['crn'];
    $chosencrn = $crn;
} else if (isset($_GET['crn'])) {
    $crn = $_GET['crn'];
    $chosencrn = $crn;
} else {
    $firsttime = true;
    // We may want to load the first thing under "now running" in this case.
}

include('weeks.php');

?>


<script type="text/javascript">
    <!--

    var slist = new Array();
    var spos = new Array();


    function loadStudents(id) {
        location.href = "<?php echo $_SERVER['PHP_SELF']?>?" + id
    }

    function setupThings() {
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (needle) {
                for (var i = 0; i < this.length; i++) {
                    if (this[i] === needle) {
                        return i;
                    }
                }
                return -1;
            };
        }

        document.getElementById("sid").focus();

        <?php
            // If we have been called we should have a crn. We load the
            // student list.

          if (!$firsttime) {
          // We load the array slist;
            $qry = "SELECT * from students where student_number in (SELECT distinct student_number from student_timetable WHERE crn = '$chosencrn')";
            $result = $db->query($qry)->rows;
            foreach ($result as $row) {
              echo "  slist[\"" . $row['student_number'] . "\"] = \"" . $row['firstname'] . " "
            . $row['lastname'] . "\";\n";
            }
          }

          $student_postion_sql = "SELECT students.student_id, students.firstname, students.lastname, students.student_number, sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent from students, sobs where (sobs.sob_id not in (select sob_observations.sob_id from sob_observations where sob_observations.student_id = students.student_id))  and sobs.expected_completion_date < CURDATE() group by students.student_id
        UNION
        SELECT students.student_id, students.firstname, students.lastname, students.student_number, sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent from students, sobs where students.student_id not in (select distinct sob_observations.student_id from sob_observations)  and sobs.expected_completion_date < CURDATE() group by students.student_id";

             $student_postion_query = $db->query($student_postion_sql)->rows;
             foreach ($student_postion_query as $stud_pos){
                 $spos = " spos[\"" . $stud_pos['student_number'] . "\"] = \"";
                if($stud_pos['threshold']!="0"){
                    $spos.= '<b>Threshold</b> <img src=\"images/danger.png\"> (' . $stud_pos['threshold'] .')&nbsp;&nbsp;|&nbsp;&nbsp;';
                }
                else{
                    $spos.= '<b>Threshold</b> <img src=\"images/tick.png\"> (' . $stud_pos['threshold'] .')&nbsp;&nbsp;|&nbsp;&nbsp;';
                }

                if($stud_pos['typical']!="0"){
                    $spos.= '<b>Typical</b> <img src=\"images/danger.png\"> (' . $stud_pos['typical'] .')&nbsp;&nbsp;|&nbsp;&nbsp;';
                }
                else{
                    $spos.= '<b>Typical</b> <img src=\"images/tick.png\"> (' . $stud_pos['typical'] .')&nbsp;&nbsp;|&nbsp;&nbsp;';
                }


                if($stud_pos['excellent']!="0"){
                    $spos.= '<b>Excellent</b> <img src=\"images/danger.png\"> (' . $stud_pos['excellent'] .')&nbsp;&nbsp;';
                }
                else{
                    $spos.= '<b>Excellent</b> <img src=\"images/tick.png\"> (' . $stud_pos['excellent'] .')&nbsp;&nbsp;';
                }

                $spos.="\";\n";
                echo $spos;
             }
        ?>

    }

    window.onload = setupThings;

    var students = new Array();
    var counter = 0;


    function addElement() {
        var ni = document.getElementById('myDiv');
        var studid = document.getElementById('sid').value;
        studid = studid.toUpperCase();
        document.getElementById('sid').disabled = true;
        if (studid.length == 0) {
            return false;
        }

        if (studid.length > 9) {
            alert(studid + ': Student ID too long, it cannot contain more than 9 characters');
            return false;
        }

        var validsequence = /^[a-zA-Z0-9]*$/.test(studid);

        if (!validsequence) {
            alert(studid + ' contains invalid characters. Only letter and numbers are allowed for student ID');
            return false;
        }

        var w = $('#week').val();

        if (document.getElementById('nowcrn').value == "other") {
            var c = $('#crn').val();
        }
        else if (document.getElementById('crn').value == "other") {
            var c = $('#nowcrn').val();
        }

        if (students.indexOf(studid) == -1) {
            var newdiv = document.createElement('div');
            var divIdName = 'Div' + studid;
            newdiv.setAttribute('id', divIdName);
            newdiv.setAttribute('class', 'individual_student');
            var studidAndName = studid;

            if (slist.hasOwnProperty(studid)) {
                studidAndName = studid + " - " + slist[studid];
                studidAndName = studidAndName.substring(0, 40);
            } else {
                studidAndName = '<font color="#FF0000">' + studid + '</font>';
            }
            if (c == "other") {
                alert('Please select a CRN to proceed');
                return false;
            }

            $.post('submit_individual_attendance.php', {week: w, crn: c, studid: studid}, function (response) {
                if (spos[studid])
                    newdiv.innerHTML = '<a href="observation.php#' + studid + '" target="_blank">' + studidAndName + '</a><div class="selected_stud_option">' + spos[studid] + ' <a style="color:#cc0000; font-weight:bold;" href=\'#\' onclick=\'removeElement("' + divIdName + '")\'>DEL</a><div>';
                else
                    newdiv.innerHTML = studidAndName + '<div class="selected_stud_option"><a style="color:#cc0000; font-weight:bold;" href=\'#\' onclick=\'removeElement("' + divIdName + '")\'>DEL</a><div>';
                ni.insertBefore(newdiv, myDiv.firstChild);
                students.push(studid);
                document.getElementById('myDiv').style.display = "block";
                document.getElementById('counter').innerHTML = parseInt(document.getElementById('counter').innerHTML) + 1;
                document.getElementById('sid').disabled = false;
            })
                .done(function() {
                    document.getElementById('sid').focus();    // put the focus back on the text box so it can be easily used with barcode scanner readers
                })

                .fail(function () {
                    alert("Please check your network connection");
                    document.getElementById('sid').disabled = false;
                });


        } else {
            alert(studid + ' already registered for this session!');
        }


        document.getElementById('sid').value = "";
        return false;
    }

    function removeElement(divNum) {
        // Removing from array:
        var studid = divNum.substr(3);
        var idx = students.indexOf(studid); // Find the index
        if (idx != -1) students.splice(idx, 1);

        // Removing from screen:
        var d = document.getElementById('myDiv');
        var toberemoved = document.getElementById(divNum);


        var w = $('#week').val();
        if (document.getElementById('nowcrn').value == "other") {
            var c = $('#crn').val();
        }
        else if (document.getElementById('crn').value == "other") {
            var c = $('#nowcrn').val();
        }

        if (c == "other") {
            alert('Please select a CRN to proceed');
            return false;
        }

        $.post('delete_individual_attendance.php', {week: w, crn: c, studid: studid});


        d.removeChild(toberemoved);
        document.getElementById('counter').innerHTML = parseInt(document.getElementById('counter').innerHTML)
            - 1;

        if (parseInt(document.getElementById('counter').innerHTML) == 0) {
            document.getElementById('myDiv').style.display = "none";
        }
    }


    function submitdata() {

        var mcrnnow = document.getElementById('nowcrn').value;
        var mcrn = document.getElementById('crn').value;

        if ((mcrnnow == "other") && (mcrn == "other")) {
            alert('You need to specify a CRN before submitting');
            return false;
        }

        if (parseInt(document.getElementById('counter').innerHTML) == 0) {
            alert('You need to enter at least a student before submitting');
            return false;
        }


        var myForm = document.createElement("form");
        myForm.method = "post";
        myForm.action = "submit_attendance.php";
        var L = students.length;
        var i = 0;
        var mybigstring = "";
        while (i < L) {
            mybigstring += students[i];
            i++;
            if (i < L) {
                mybigstring += ";";
            }
        }

        var myInputnow = document.createElement("input");
        var mycrnnow = document.getElementById('nowcrn').value;
        myInputnow.setAttribute("name", "sendnowcrn");
        myInputnow.setAttribute("value", mycrnnow);
        myForm.appendChild(myInputnow);

        var myInput2 = document.createElement("input");
        var mycrn = document.getElementById('crn').value;
        myInput2.setAttribute("name", "sendcrn");
        myInput2.setAttribute("value", mycrn);
        myForm.appendChild(myInput2);


        var myInput4 = document.createElement("input");
        var myweek = document.getElementById('week').value;
        myInput4.setAttribute("name", "sendweek");
        myInput4.setAttribute("value", myweek);
        myForm.appendChild(myInput4);

        var myInput = document.createElement("input");
        myInput.setAttribute("name", "rawdata");
        myInput.setAttribute("value", mybigstring);
        myForm.appendChild(myInput);


        document.body.appendChild(myForm);
        myForm.submit();
        document.body.removeChild(myForm);
    }


    //-->
</script>

<?php $cweek = curweek($db); // see above, this is the current week ?>
<title>Week: <?php echo $cweek; ?></title>

<div id="wrapper">
    <div id="wrapper_content">
        <form name="selectcrn" id="crn_frm" class="f-wrap-1">
            <table cellpadding="5" cellspacing="5" width="100%">
                <tr>
                    <td width="125">Now running CRN:</td>
                    <td>
                        <select id="nowcrn"
                                onChange="document.getElementById('sid').focus(); loadStudents(&quot;nowcrn=&quot;+this.value);">
                            <option value="other">OTHER (please specify) --></option>
                            ";

                            <?php
                            $lowerbound = time() + 10 * 60; // 10 minutes ago;
                            $upperbound = time() - 10 * 60; // time in 10 mins
                            $dow = date("l"); // day of week, e.g. Friday

                            // a 10 min grace period before and after
                            $lhour = date("H", $lowerbound); // hour, e.g. 09 or 15
                            $lmin = date("i", $lowerbound); // mins, e.g. 04 or 42
                            $uhour = date("H", $upperbound); // hour, e.g. 09 or 15
                            $umin = date("i", $upperbound); // mins, e.g. 04 or 42

                            $qry_crn = "SELECT C.crn as crn,C.codetype as codetype,C.day as day,C.room as room,C.starttime as starttime,C.endtime as endtime from
                    CRNlist C , staff_timetable S  
                    where S.staff_id ='$uid'
                    and S.crn = C.crn
                    and day like '$dow'
                    and starttime <= '" . $lhour . $lmin . "'
                    and endtime >= '" . $uhour . $umin . "'
                    order by starttime";


                            $result = $db->query($qry_crn)->rows;
                            foreach ($result as $row) {
                                echo "<option value=\"" . $row['crn'] . "\"";
                                if (!$firsttime && (isset($_REQUEST['nowcrn']))) {
                                    if ($chosencrn == $row['crn']) {
                                        echo " selected";
                                    }
                                }
                                echo ">" . "(" .
                                    $row['codetype'] . "," . $row['crn'] . ") " . $row['room'] . ", " .
                                    $row['starttime'] . "-" . $row['endtime'] . "</option>";
                            }
                            ?>

                        </select>
                    </td>
                    <td width="150" align="right" valign="top">
                        <span style="font-size:20px;font-weight:bold;float:left;">Total:</span>

                        <div id="counter" style="height:50px;width:80px;font-size:25px;font-weight:bold;float:right;">
                            0
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>All CRNs:</td>
                    <td><select id="crn"
                                onChange="document.getElementById('sid').focus();loadStudents(&quot;crn=&quot;+this.value)">
                            <option value="other">OTHER (please specify) --></option>
                            ";

                            <?php
                            $qry = "SELECT CRNlist.crn as crn,codetype,day,room,starttime,endtime FROM CRNlist order by starttime";

                            $result = $db->query($qry)->rows;
                            foreach ($result as $row) {
                                echo "<option value=\"" . $row['crn'] . "\"";
                                if (!($firsttime) && (isset($_REQUEST['crn']))) {
                                    if ($chosencrn == $row['crn']) {
                                        echo " selected";
                                    }
                                }
                                echo ">" . "(" .
                                    $row['codetype'] . ") " . $row['room'] . ", " . substr($row['day'], 0, 3) . " " .
                                    $row['starttime'] . "-" . $row['endtime'] . "</option>";
                            }
                            ?>

                        </select>
                    </td>
                    <td align="right">

                    </td>
                </tr>
                <tr>
                    <td>Week</td>
                    <td>
                        <select id="week">
                            <?php
                            for ($i = 1; $i <= 24; $i++) {
                                echo "<option value=\"$i\"";
                                if ($cweek == $i) {
                                    echo " selected";
                                }
                                echo ">$i</option>\n";
                            }
                            ?>
                        </select>
                    </td>
                    <td align="center">&nbsp;</td>
                </tr>
            </table>


        </form>

        <form onSubmit="return addElement();">
            <input type="hidden" value="0" id="theValue"/>
            <table width="500" border="0" align="center" cellpadding="5">
                <tbody>
                <tr>
                    <td width="144"> Student Number</td>
                    <td width="256" align="right"><input type="text"
                                                         style="height:23px;width:250px;border:1px solid #CCC" size="15"
                                                         name="sid" id="sid"></td>
                </tr>
                </tbody>
            </table>
        </form>

        <div id="myDiv" style="display:none;">
        </div>

    </div>

</div>
<?php
include 'footer.php';
?>
