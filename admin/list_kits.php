<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
include_once('update_overdue_kits.php');
extract($_GET);



?>
<div id="sobs_filtered_list">
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th align="left">Asset Tag</th>
          <th align="left">Kit Type</th>
          <th align="left">Status</th>
          <th align="left">Notes</th>
          <?php
          if(has_capabilities($uid, 'Kits')==true){
		  ?>
          <th width="80" align="left">Edit</th>
          <?php
		  }
		  ?>
      </tr>
<?php
$query="SELECT DISTINCT kits.kit_id, kits.kit_asset_tag, kit_type.kit_type_name, kit_status.kit_status_name, kits.kit_notes FROM kits LEFT OUTER JOIN kit_type ON kits.kit_type_id = kit_type.kit_type_id LEFT OUTER JOIN kit_status ON kits.kit_status_id = kit_status.kit_status_id LEFT OUTER JOIN kit_loans ON kits.kit_id = kit_loans.kit_id WHERE 1=1";



if(isset($filter_kit_types))
{
     $filter_kit_types= implode(',',$filter_kit_types);
	 $query .= " and kits.kit_type_id in ( $filter_kit_types ) ";
}
if(isset($filter_kit_status))
{
     $filter_kit_status= implode(',',$filter_kit_status);
	 $query .= " and kits.kit_status_id in ( $filter_kit_status ) ";
}

if(isset($filter_kit_note))
{
    $query .= " and kits.kit_notes != ''";
}

$query .= " GROUP BY kit_id ORDER BY kit_loans.kit_checked_out_at DESC";

$kits_obj = $db->query($query);
$kits_no = $kits_obj->num_rows;

if($kits_no!=0){
	$kits = $kits_obj->rows;
	$s=0;
	foreach($kits as $kit){
	$s++;
	?>
    <tr id="sob_tr_<?php echo $kit['kit_id'];?>">
          <td align="left" valign="top"><?php echo $kit['kit_asset_tag'];?></td>
          <td align="left" valign="top"><?php echo $kit['kit_type_name'];?></td>
          <td valign="top" align="left"><?php echo $kit['kit_status_name'];?></td>
          <td valign="top" align="left"><?php echo $kit['kit_notes'];?></td>
          
          <?php
          if(has_capabilities($uid, 'Kits')==true){
		  ?>
          <td align="left" valign="top">
          <a href="javascript:;" onclick="edit_kit('<?php echo $kit['kit_id'];?>')" title="Edit Kit"><img src="images/edit.png" border="0" alt="Edit Kit" /></a> &nbsp;&nbsp;
          <a href="javascript:;" onclick="delete_kit('<?php echo $kit['kit_id'];?>')" title="Delete Kit"><img src="images/nocapability.png" border="0" alt="Delete Kit" /></a> &nbsp;&nbsp;
          <a href="javascript:;" onclick="kit_history('<?php echo $kit['kit_id'];?>')" title="Kit History"><img src="images/notes.png" border="0" alt="Kit History" /></a>
          </td>
          <?php
		  }
		  ?>
         
      </tr>
    <?php
	}
}
else{
	?>
     <tr>
        <Td align="center" colspan="7"><br /><b>-- No results found --</b></Td>
        </tr>
    <?php
}
?>
</table>
</div>
