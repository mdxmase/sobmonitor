<?php
session_start();
include 'login_checker.php';
include 'header.php';


?>

<link class="include" rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
<script src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>

<script>

$(document).ready(function() {
	show_all_student_record_compare();
	$(document).bind('click', function(e) {
	var $clicked = $(e.target);
	if (!$clicked.parents().hasClass("filter_wrapper") && !$clicked.hasClass("ui-state-default") && !$clicked.hasClass("ui-icon") && !$clicked.hasClass("ui-datepicker-month") && !$clicked.hasClass("ui-datepicker-title") && !$clicked.hasClass("ui-datepicker-div") && !$clicked.hasClass("ui-datepicker") && !$clicked.hasClass("ui-datepicker-header") && !$clicked.parent().parent().parent().parent().hasClass("ui-datepicker-calendar") && !$clicked.parent().parent().parent().parent().hasClass("ui-datepicker")){
			$('.filter_holder').css('display','none');
		}
	});
});

function open_filter(level){
		if($('#filter_holder_'+level).css('display')=='block'){
			$('#filter_holder_'+level).css('display','none');
		}
		else{
			$('#filter_holder_'+level).css('display','block');
			$('.datepicker').datepicker();
		}
}

function filter_graph_overall(type){
	var flag = 0;
	
	var levels = "";var topics = "";var sdate = ""; var edate = ""; var from_observed = ""; var to_observed = ""; var include = ""; var from_week = ""; var to_week = "";
	
	var level_arr = document.getElementsByName('filter_levels');
	for(var i=0;i<level_arr.length;i++){
		if(level_arr[i].checked==true){
			if(levels=="")
			levels = level_arr[i].value;
			else
			levels+=","+level_arr[i].value;
			
			flag=1;
			
		}
	}
	
	
	var arr = document.getElementsByName('filter_topics');
	for(var i=0;i<arr.length;i++){
		if(arr[i].checked==true){
			if(topics=="")
			topics = arr[i].value;
			else
			topics+=","+arr[i].value;
			
			flag = 1;
		}
	}
	
	if(document.getElementById('start_date_from').value!=""){
		flag=1;
		sdate = document.getElementById('start_date_from').value;
		
		if(document.getElementById('start_date_to').value==""){
			$('#date_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
		
	}
	
	if(document.getElementById('start_date_to').value!=""){
		flag=1;
		edate = document.getElementById('start_date_to').value;
		
		if(document.getElementById('start_date_from').value==""){
			$('#date_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
	}
	
	if(document.getElementById('observed_from').value!=""){
		flag=1;
		from_observed = document.getElementById('observed_from').value;
		
		if(document.getElementById('observed_to').value==""){
			$('#observed_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
		
	}
	
	if(document.getElementById('observed_to').value!=""){
		flag=1;
		to_observed = document.getElementById('observed_to').value;
		
		if(document.getElementById('observed_from').value==""){
			$('#observed_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
	}
	

	if(document.getElementById('from_week').value!=""){
		flag=1;
		from_week = document.getElementById('from_week').value;
		
		if(document.getElementById('to_week').value==""){
			$('#weekrange_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
		
	}


	if(document.getElementById('to_week').value!=""){
		flag=1;
		to_week = document.getElementById('to_week').value;
		
		if(document.getElementById('from_week').value==""){
			$('#observed_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
	}


	if(document.getElementById('include_inactive').checked==true){
		include = '&inactive=1';
		flag=1;
	}
	else{
		include = '';
	}
	
	
	if(flag==1){
		var filters = 'levels='+levels+'&topics='+topics+'&from_date='+sdate+'&to_date='+edate+'&observed_from='+from_observed+'&observed_to='+to_observed+'&from_week='+from_week+'&to_week='+to_week+include;
	}
	else{
		var filters ="";
	}
	
	document.getElementById('filter_query').value=filters;
	var url = 'show_student_record_compare_dashboad.php?'+filters;
	$.getJSON(url, function(data){
		open_filter(type);
		generate_graph(data.values, data.labels, data.colors, 'chart', data.applied_filter,data.max_y_value)
	});
		
	
}

function generate_graph(values, labels, colors, holder, applied_filter,max_y_value){
	$.jqplot.config.enablePlugins = true;
			var s1 = values;
			var ticks = labels;
			
			plot1 = $.jqplot(holder, [s1], {
				// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
				animate: !$.jqplot.use_excanvas,
				seriesColors:colors,
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					shadow : false,
					rendererOptions: {
					// Set varyBarColor to tru to use the custom colors on the bars.
					varyBarColor: true
				},
					pointLabels: { show: false }
				},
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks,
						label : 'Progress of all students Vs Progress expected by <?php echo date('d.m.Y');?>',
						labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					},
					yaxis:{
						tickOptions:{
						formatString: "%d",
						fontSize: '12px'
						},
					   label:'Number of SOBs',
					   	min:0,
  				    	max: max_y_value,
						 
					   labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					   labelOptions: {
							fontFamily: 'Arial',
							fontSize: '14px'
						  }
					}
				},
				highlighter: { show: true }
			});
			plot1.replot();
			$('#graph_filter_info').html(applied_filter);
}

function show_all_student_record_compare(){
	var url = 'show_student_record_compare_dashboad.php';
	$.getJSON(url, function(data){
		generate_graph(data.values, data.labels, data.colors, 'chart', data.applied_filter,data.max_y_value)
	});
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode
	
	if (charCode ==13)
	check_student_id_valid();
	
	return true;
}

function print_filter_dashboard(){
	var filters = document.getElementById('filter_query').value;
	var url = 'print_filter_dashboard.php?'+filters;
	window.open(url, "PRINT_FILTER", "menubar=1,resizable=1,scrollbars=1,width=1000,height=700,left=10,top=10");
}


</script>
<link href="css/dashboard.css" rel="stylesheet" type="text/css" /> 
<br />
<br />
<div id="wrapper">
    <div id="wrapper_content">
    	
        <div id="content" style=" position:relative;">
        
        <div class="widget-box widget-plain">
      <div class="center">
        <ul class="stat-boxes2">
        
        <?php
		$sob_obj = $db->query("SELECT * FROM `sobs` WHERE 1");
		$sob_no = $sob_obj->num_rows;
        ?>
          <li><a href="sobs.php"><div class="right"> <strong><?php echo $sob_no;?></strong> Total SOBs </div></a></li>
         	<?php
			// HAS MANAGE STAFF //
			if(has_capabilities($uid,'Manage Staffs')==true){
				$staffs_obj = $db->query("SELECT * FROM `staffs` WHERE 1");
				$staffs_no = $staffs_obj->num_rows;
				?>
				<li><a href="staffs.php"><div class="right"> <strong><?php echo $staffs_no;?></strong> Total Staff</div></a></li>
				<?php
			}
			
			// HAS MANAGE STUDENTS //
			 if(has_capabilities($uid,'Manage Students')==true){
				$students_obj = $db->query("SELECT * FROM `students` WHERE student_status = 0");
				$students_no = $students_obj->num_rows;
				?>
				<li><a href="students.php"><div class="right"> <strong><?php echo $students_no;?></strong> Total Students </div></a></li>
				<?php
			 }
			 ?>
        </ul>
      </div>
    </div>
        
        <?php $legend_graph = '<div id="graph_legend">
                <ul>
					<li><a href="javascript:;" onclick="print_filter_dashboard()"><div class="legend_key print"></div>Print</a><input name="filter_query" id="filter_query" type="hidden" value="" /></li>  
					<li><div class="legend_key other_students"></div>All students</li>
					<li><div class="legend_key ideal"></div>Expected by '.date('d.m.Y').'</li>                
                </ul>
            </div>'; 
			$filter = '<div class="filter_wrapper"><b onClick="open_filter(1)" class="highlight_color">Filter</b> 
				<div class="filter_holder highlight_color" id="filter_holder_1">
				<strong>Levels</strong> <a href="javascript:;" class="close_button" onClick="open_filter(1)" style="float:right;">X</a><br />
        				<input type="checkbox" value="1" name="filter_levels" id="level"> Threshold
						<input type="checkbox" value="2" name="filter_levels" id="level"> Typical
						<input type="checkbox" value="3" name="filter_levels" id="level"> Excellent <br>

				<strong>Topics</strong><br /><ul class="filter_list">';
				
		$topics_obj = $db->query("select * from `topics` where 1");
		$topics = $topics_obj->rows;
		
		foreach($topics as $topic){
			$filter.='<li><input type="checkbox" value="'.$topic['topic_id'].'" name="filter_topics" id="topic"> ' . $topic['topic'] . '</li>';
			
		}
					
							
					$filter.='</ul><div class="clear"></div>
					<strong>Expected Completion Date</strong><br />
                <input type="text" style="width:80px;" placeholder="From" class="datepicker" name="start_date_from" id="start_date_from" onFocus="document.getElementById(\'date_filter_status\').innerHTML=\'\'">&nbsp;&nbsp;
                <input type="text" style="width:80px;" placeholder="To" class="datepicker" name="start_date_to" id="start_date_to" onFocus="document.getElementById(\'date_filter_status\').innerHTML=\'\'"><br /><span id="date_filter_status" class="error"></span><br />
				
				<strong>No of Observed SOBs</strong><br />
                <input type="text" style="width:60px;" placeholder="From" name="observed_from" id="observed_from" onFocus="document.getElementById(\'observed_filter_status\').innerHTML=\'\'">&nbsp;&nbsp;
                <input type="text" style="width:60px;" placeholder="To" name="observed_to" id="observed_to" onFocus="document.getElementById(\'observed_filter_status\').innerHTML=\'\'"><br /><span id="observed_filter_status" class="error"></span><br />

				<strong>Observation range</strong><br />
                <input type="text" style="width:80px;" placeholder="From date" class="datepicker" name="from_week" id="from_week" onFocus="document.getElementById(\'weekrange_status\').innerHTML=\'\'">&nbsp;&nbsp;
                <input type="text" style="width:80px;" placeholder="To date" class="datepicker" name="to_week" id="to_week" onFocus="document.getElementById(\'weekrange_status\').innerHTML=\'\'"><br /><span id="weekrange_status" class="error"></span><br />

				<input type="checkbox" id="include_inactive" name="include_inactive" value="1" /> Include inactive students<br />
				<a class="small themebutton button" href="javascript:;" onClick="filter_graph_overall(\'1\')">Submit</a><br />
				
				</div>
			</div>';
			
			

			
			?>
            <div class="canvas_holder">
                <div class="graph_display_details"><h2>Overall Progress <?php echo $filter;?></h2>
                    <div class="graph_filter_info" id="graph_filter_info"><strong>Levels</strong> : All | <strong>Topics</strong> : All <stong>Week range</strong>: All<br /><strong> Expected Completion Date</strong> : Any | <strong>No of Observed SOBs</strong> : Any</div>
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                
                <div id="chart" style=" width:100%; height:450px;"></div>
            </div>
            
    		
        </div>
    </div>
</div>

<?php
include 'footer.php';
?>