<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);


?>

<div id="suggestion_box" style="display: inline; margin: 0; padding: 0;">

<input type="hidden" id="filter_student_moid" name="filter_student_moid" value="<?php echo $student_id;?>" />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="sub_headings" align="left">FILTERS</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td><strong>Levels</strong></td>
      </tr>
      <tr>
        <td>
        <?php
		$levels_obj = $db->query("select * from `levels` where 1");
		$levels = $levels_obj->rows;
		
		foreach($levels as $level){
			?>
			<input type="checkbox" id="level_<?php echo $level['level_id'];?>" name="levels" value="<?php echo $level['level_id'];?>"> <?php echo $level['level'];?> <br>
			<?php
		}
		?>
        </td>
      </tr>
      
      <tr>
        <td><strong>Topics</strong></td>
      </tr>
      <tr>
        <td>
        <?php
		$topics_obj = $db->query("select * from `topics` where 1");
		$topics = $topics_obj->rows;
		
		foreach($topics as $topic){
			?>
			<input type="checkbox" id="topic_<?php echo $topic['topic_id'];?>" name="topics" value="<?php echo $topic['topic_id'];?>"> <?php echo $topic['topic'];?> <br>
			<?php
		}
		?>
        </td>
      </tr>
      
      <tr>
        <td><strong>Expected completion date</strong></td>
      </tr>
      <tr>
        <td>
       <input type="text" id="start_date" name="start_date" class="datepicker" placeholder="From Date" style="width:80px;" />&nbsp;&nbsp;
       <input type="text" id="end_date" name="end_date" class="datepicker" placeholder="To Date" style="width:80px;" />
        </td>
      </tr>
      
      <tr>
        <td><strong>SOB Status</strong></td>
      </tr>
      <tr>
        <td>
        <!--SELECTING NONE WOULD SHOW ALL-->
       <input type="checkbox" id="sob_status" name="sob_status" value="1" /> Observed&nbsp;&nbsp;
       <input type="checkbox" id="sob_status" name="sob_status" value="2" /> Unobserved
        </td>
      </tr>
      
      
      <tr>
        <td><strong>Keywords</strong></td>
      </tr>
      <tr>
        <td>
  		<input type="text" id="keywords" name="keywords" placeholder="Keywords" onkeyup="search_keywords(this,event)" />
        </td>
      </tr>
      <tr>
        <td><div id="sob_keywords_tags">&nbsp;</div></td>
      </tr>
      <tr>
        <td> <a class="small themebutton button" href="javascript:;" onclick="filter_sob_observe_list()">Apply Filter</a></td>
      </tr>
    </table>

</div>