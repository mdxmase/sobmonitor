<?php
session_start();
include 'login_checker.php';
include 'header.php';

if(has_capabilities($uid, 'Kits')==false){
	header('Location:home.php');
	exit();
}
?>
<script>


function show_kit_details(){
	if(document.getElementById('kit_asset_tag').value!=""){
		var kit = document.getElementById('kit_asset_tag').value;
		$('#kitDetailsForm').html('Please wait... Loading...').load('kit_prepare_loan.php?kit_asset_tag='+kit, function(){
		    if ($('#kitDetailsForm:contains("Invalid Kit Asset Tag")').length == 0) {
                document.getElementById('student_number').focus();
            } else {
                $('#kit_asset_tag').removeAttr('value');
                $('#kit_asset_tag').focus();
            }
        });

	}
}


function isEnterKeyKit(evt)
{
 var charCode = (evt.which) ? evt.which : evt.keyCode

 if (charCode ==13) {
     show_kit_details();

 }


 return true;
}

function isEnterKeyStud(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode

    if (charCode ==13)
        checkStudentKitLoans();


    return true;
}

function checkStudentKitLoans(){
    if(document.getElementById('student_number').value!=""){
        var stud = document.getElementById('student_number').value;
        stud = stud.toUpperCase();
        $.post("check_student_kits.php",
            {
                student_number: stud
            },
            function(response){
                if (response == "ok") {
                    processLoan();
                }
                else if (response == "overdue") {
                    var conf = confirm("This student has kit(s) overdue, Are you sure you want to continue?");
                    if(conf==true){
                        processLoan();
                    } else {
                        resetPage();
                    }
                }
                else if (response == "multipleKits") {
                    var conf = confirm("This student already has 2+ kits currently signed out, Are you sure you want to continue?");
                    if (conf == true) {
                        processLoan();
                    } else {
                        resetPage();
                    }
                }
                else if (response == "nonExist") {
                    alert("This student number doesn't exist");
                    $('#student_number').removeAttr('value');
                    $('#student_number').focus();
                }
            });

    }
}

function processLoan(){
    if(document.getElementById('loan_duration_length').value!=""){
        var stud = document.getElementById('student_number').value;
        stud = stud.toUpperCase();
        var durationLength = document.getElementById('loan_duration_length').value;
        var durationUnit = document.getElementById('loan_duration_unit').value;
        var kit = document.getElementById('kit_asset_tag').value;
        $.post("process_kit_loan.php",
            {
                duration_length: durationLength,
                duration_unit: durationUnit,
                student_number: stud,
                kit_tag: kit
            },
            function(response){
                if (response == "success") {
                    resetPage();
                    $.jGrowl("Kit successfully checked out.");
                }
                else {
                    $.jGrowl("An error occured.");
                }
            });

    }
}

function resetPage() {
    $('#kitDetailsForm').empty();
    $('#kit_asset_tag').removeAttr('value');
    $('#kit_asset_tag').focus();
}



</script>
<div id="wrapper">
    <div id="wrapper_content" style="min-width:900px;">
    	<h1 class="page_title">Check Out Kit</h1>
         <table width="100%" cellpadding="0" cellspacing="5px">
         	<tr>
              <td colspan="2"><strong>Kit Asset Tag :</strong> <input type="text" id="kit_asset_tag" name="kit_asset_tag" placeholder="Kit Tag" onkeypress="return isEnterKeyKit(event)" autofocus /> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="show_kit_details();">Go</a> </td>
            </tr>
            <tr>
              <td colspan="2"><div id="kitDetailsForm"></div></td>
            </tr>
        </table>
    </div>
    	
</div>



<?php
include 'footer.php';
?>