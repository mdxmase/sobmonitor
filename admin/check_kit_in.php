<?php
session_start();
include 'login_checker.php';
include 'header.php';

if(has_capabilities($uid, 'Kits')==false){
	header('Location:home.php');
	exit();
}
?>
<script>

function isNumberKey(evt)
{
 var charCode = (evt.which) ? evt.which : evt.keyCode

 if (charCode ==13)
	check_in_kit();

 return true;
}




function check_in_kit(){
    if(document.getElementById('kit_tag').value!="") {
        var kit = document.getElementById('kit_tag').value;
        $.post("process_kit_check_in.php",
            {
                kit_tag: kit
            },
            function(response){
                $('#kit_tag').removeAttr('value');
                $('#content').prepend(response);
            });
    }
}





</script>
<div id="wrapper">
    <div id="wrapper_content" style="min-width:900px;">
    	<h1 class="page_title">Check Kit In</h1>
         <table width="100%" cellpadding="0" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Kit Asset Tag :</strong> <input type="text" id="kit_tag" name="kit_tag" placeholder="Kit Tag" onkeypress="return isNumberKey(event)" autofocus/> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="check_in_kit()">Submit</a> </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <br>
		<br>
        <div id="content">

        </div>
    </div>
    	
</div>



<?php
include 'footer.php';
?>