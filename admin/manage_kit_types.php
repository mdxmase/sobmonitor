<?php
session_start();
include 'login_checker.php';
include 'header.php';

if(has_capabilities($uid, 'Kits')==false){
	header('Location:home.php');
	exit();
}


?>

<div id="wrapper">
    <div id="wrapper_content">
    <h1 class="page_title">Manage Kit Types</h1>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
            	<td class="sub_headings" align="left">List of Kit Types</td>
                <td align="right"> <a class="small themebutton button" style="float:right;" onClick="add_new_kit_type()" href="javascript:;">Add Kit Type</a></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
      


        <div id="page_contents">
            <?php
            include 'list_kit_types.php';
            ?>
      </div>
      
  </div>
</div>
<?php
include 'footer.php';
?>