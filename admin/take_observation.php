<?php
session_start();
include_once('login_checker.php');

$stud_id = $_GET['student_id'];
$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_number` = '$stud_id'");
$stud_no = $stud_obj->num_rows;
extract($_GET);
if($stud_no==0){
	
	?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1">
		 <tr>
			<Td align="center"><b style="color:#F00">Invalid Student ID</b></Td>
		  </tr>
		</table>
    <?php
	
}
else{
	$stud_details = $stud_obj->row;
	$student_id = $stud_details['student_id'];
	
	$today = date('Y-m-d');
	$week = date('Y-m-d',strtotime("+7 days"));
	?>
    <div class="student_individual_graph_holder highlight_color">
    <table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="sub_headings" align="left">Overall progress as on <?php echo date_ft($today);?> -  <span><?php echo $stud_details['firstname'];?> <?php echo $stud_details['lastname'];?> (<?php echo $stud_details['student_number'];?>)</span>  </td>
                    <td><a href="javascript:;" class="close_button" onclick="toggle_graph('student_individual_graph',this)">+ Show</a></td>
				</tr>
           </table>
           
           <div  id="student_individual_graph" style="display:none;">
           <br />
    <?php
	
	
	
		$level_obj = $db->query("select DISTINCT l.level_id, l.level from `sobs` s, `levels` l where s.level_id = l.level_id ORDER BY l.level_id");
		$level_arrs = $level_obj->rows;
		
		foreach($level_arrs as $level){
			$level_id = $level['level_id'];
			$total_obj = $db->query("SELECT COUNT(*) AS total FROM `sobs` s, `levels` l WHERE s.level_id = l.level_id AND l.level_id = '$level_id'");
			$tot = $total_obj->row;
			
			$user_tot_obj = $db->query("SELECT COUNT(*) AS user_total FROM `sobs` s, `levels` l WHERE s.level_id = l.level_id AND l.level_id = '$level_id' AND s.expected_completion_date < '$today'");
			$user_tot = $user_tot_obj->row;
			
			$user_finished_obj = $db->query("SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` IN (SELECT sob_id FROM `sobs` WHERE `level_id` = '$level_id') AND (observed_on <= '$today' AND observed_on != '0000-00-00')");
			$user_finished = $user_finished_obj->row;
			
			$total = $tot['total']; 
			$user_total = $user_tot['user_total']; 
			$user_finish = $user_finished['user_finished_total'];
			
			$achieved = ($user_finish/$total)*100;
			$achievement = ($user_total/$total)*100;
			?>
            <b class="level_display"><?php echo $level['level'];?></b>
            <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : ' . $total;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed : ' . $user_finish;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected by '.date_ft($today).' : ' . $user_total;?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
            <?php
		}

	?>
    </div>
    </div>
    <br />
<br />
<br />
<table width="600" cellpadding="0" cellspacing="0">
				<tr>
					<td class="sub_headings" align="left">LIST OF SOBS FOR <span><?php echo $stud_details['firstname'];?> <?php echo $stud_details['lastname'];?> (<?php echo $stud_details['student_number'];?>)</span> </td>
				</tr>
           </table>
           <br />
<br />

			
	<?php
	$sql = "SELECT s.sob_id, s.sob, s.level_id, l.level, s.topic_id, t.topic, s.expected_completion_date FROM `sobs` s, `levels` l, `topics` t WHERE s.level_id = l.level_id and s.topic_id = t.topic_id";
	
	if($levels!=""){
		$sql.=" AND l.level_id IN ($levels)";
	}
	
	if($topics!=""){
		$sql.=" AND t.topic_id IN ($topics)";
	}
	
	if($from_date!=""){
		$from_date = date_mysql($from_date);
		$sql.=" AND s.expected_completion_date >= '$from_date' ";
	}
	
	if($to_date!=""){
		$to_date = date_mysql($to_date);
		$sql.=" AND s.expected_completion_date <= '$to_date' ";
	}
	
	if($keywords_sob_id!=""){
		$sql.=" AND s.sob_id IN ($keywords_sob_id) ";
	}
	
	if($sob_status!="" && $sob_status!="1,2"){
		if($sob_status=="1"){
			$sql.=" AND s.sob_id IN (SELECT sob_id FROM `sob_observations` WHERE `student_id` = '$student_id') ";
		}
		else if($sob_status=="2"){
			$sql.=" AND s.sob_id NOT IN (SELECT sob_id FROM `sob_observations` WHERE `student_id` = '$student_id')";
		}
	}
	
	$sql.=' ORDER BY s.level_id, s.topic_id ';
	
	$sobs_obj = $db->query($sql);
	$sobs_no = $sobs_obj->num_rows;
	
	$divnames=1;
	
	$total = 0;
	$expected = 0;
	$observed = 0;
	
	if($sobs_no!=0){
		$sobs = $sobs_obj->rows;
		
		$topic_name = "";
		$level_name = "";
		
		$level_arr = array();
		
		?>
        
        <table width="600" cellpadding="0" cellspacing="0">
	
				
				<tr>
                  <td align="left"><strong>ECD</strong> - <strong>E</strong>xpected <strong>C</strong>ompletion <strong>D</strong>ate</td>
				  <td align="right">
                  <ul class="observe_legend">
                  	<li class="color_box sob_expired">&nbsp;</li>
                    <li>Overdue</li>
                    <li class="color_box sob_observed">&nbsp;</li>
                    <li>Observed</li>
                    <li class="color_box sob_expire_today">&nbsp;</li>
                    <li>Active </li>
                  </ul>
                  </td>
				</tr>
	
			</table>
        
		<table width="600" border="0" cellpadding="10" cellspacing="1">
		<?php
		$s=0;
		foreach($sobs as $sob){
		
		$sob_id = $sob['sob_id'];
		
		if($level_name!=$sob['level']){
	
		
			
			
			?>
            
           
            
			<tr>
				<td class="level_name" colspan="2"><?php echo $sob['level'];?></td>
			</tr>
             <tr>
            	<td colspan="2">
                
                <div class="process_content_bar" id="<?php echo $divnames++;?>">
                <?php
				$achieved = ($observed/$total)*100;
				$achievement = ($expected/$total)*100;
				?>
                <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : <b>' . $total .'</b>';?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed :  <b>' . $observed.'</b>';?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected by '.date_ft($today).' : <b>' . $expected.'</b>';?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
                
                </div>
                
                </td>
            </tr>
			<?php
			$total = 0;
	$expected = 0;
	$observed = 0;
			$level_name=$sob['level'];
		}
		
		if($topic_name!=$sob['topic']){
			?>
			<tr>
				<td class="topic_name" colspan="2"><?php echo $sob['topic'];?></td>
			</tr>
			<?php
			$topic_name=$sob['topic'];
			$s=0;
		}
		
		$s++;
		$total++;
		
		if($sob['expected_completion_date']<$today){
			$expected++;
		}
		
		$obs_obj = $db->query("SELECT * FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id'");
		$obs_no = $obs_obj->num_rows;
		$obs_row = $obs_obj->row;
		
		$notes_obj = $db->query("SELECT * FROM `sob_notes` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id' AND `public` = 1");
		$notes_no = $notes_obj->num_rows;
		
		$priv_notes_obj = $db->query("SELECT * FROM `sob_notes` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id' AND `public` = 0");
		$priv_notes_no = $priv_notes_obj->num_rows;
		?>
		<tr class="sob_highlight">
			  <td align="left" <?php if($sob['expected_completion_date']<$today && $obs_no==0)  echo 'class="sob_expired"';  else if($sob['expected_start_date']<$today && $sob['expected_completion_date']>$today && $obs_no==0)  echo 'class="sob_expire_today"'; else if($obs_row['observed_on']!='0000-00-00' && $obs_row['observed_on']!='') echo 'class="sob_observed"'; ?> width="10"><?php echo $sob_id;?></td>
			  <td align="left"><?php echo $sob['sob'];?></Td>
		</tr>
			<tr>      
			  <td colspan="2">
			  <div style="float:left;"><strong>ECD:</strong> <?php echo date_ft($sob['expected_completion_date']); if($sob['expected_completion_date']<$week && $obs_no==0 && $sob['expected_completion_date']>=$today) { echo '  <b>'. DayDifference($today, $sob['expected_completion_date']) . ' day(s) left</b>'; } ?></div> 
              
              <div style="float:right;">
              Notes :
              <a class="small green button" id="notes_btn_<?php echo $sob_id;?>" onClick="sob_notes('<?php echo $sob_id;?>','<?php echo $student_id;?>')" href="javascript:;">Public (<?php echo $notes_no;?>)</a> &nbsp; <a class="small green button" id="private_notes_btn_<?php echo $sob_id;?>" onClick="sob_notes_private('<?php echo $sob_id;?>','<?php echo $student_id;?>')" href="javascript:;">Private (<?php echo $priv_notes_no;?>)</a>
              </div>
              <?php
			  if($obs_no==0){
				?>
                	<div style="float:right;padding-right:10px;" id="sob_status_<?php echo $sob['sob_id'];?>"><a class="small themebutton button" onClick="observed('<?php echo $sob['sob_id'];?>','<?php echo $student_id;?>')" href="javascript:;">Observe</a></div>
				 <?php
				}
				else{
				$staffid = $obs_row['observed_by'];
				$obs_id = $obs_row['observation_id'];
				
					if($staffid!="0"){
						$teacher_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staffid'");
						$teacher_details = $teacher_obj->row;
						$teacher_name = $teacher_details['firstname'] . " " . $teacher_details['lastname'];
						$observed_by = ' by ' . $teacher_name;
						
					}
					else{
						$observed_by = '';
					}
				$observed++;
				
					if($staffid==$uid){
						$undo = '<a class="small themebutton button" onClick="undo_observed(\''.$obs_id.'\',\''.$sob_id.'\')" href="javascript:;">Undo</a>';
					}
					else{
						$undo = '';
					}
				
					if($obs_row['observed_on']!='0000-00-00'){
					 echo '<div style="float:right;padding-right:10px;" id="sob_status_'.$sob_id.'"><strong>Observed on</strong> : ' . date_ft($obs_row['observed_on']). $observed_by . '  &nbsp;&nbsp; '.$undo.'</div>';
					}
					else{
					?>
                    	<div style="float:right;padding-right:10px;" id="sob_status_<?php echo $sob['sob_id'];?>"><a class="small themebutton button" onClick="observed('<?php echo $sob_id;?>','<?php echo $student_id;?>')" href="javascript:;">Observe</a>&nbsp;&nbsp;</div>
					<?php
					}
				
				}
				?>
			  			
              
			  </td>
			</tr>
		<?php
		}
		?>
        <tr>
        	<td colspan="2"><div class="process_content_bar" id="<?php echo $divnames++;?>">
             <?php
				$achieved = ($observed/$total)*100;
				$achievement = ($expected/$total)*100;
				?>
                <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : ' . $total;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed : ' . $observed;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected by '.date_ft($today).' : ' . $expected;?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
            </div></td>
        </tr>
	</table>
    <script>
	for(var i=1;i<=<?php echo $divnames;?>;i++){
		var j = parseInt(i)+1
		$('#'+i).html($('#'+j).html());
	}
	</script>
		<?php
	}
	else{
		?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1" align="left">
		 <tr>
			<Td align="center"><b>-- No results found --</b></Td>
		  </tr>
		</table>
		<?php
	}
}
?>