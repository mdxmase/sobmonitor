<?php
session_start();
include 'login_checker.php';
include 'header.php';


if(has_capabilities($uid, 'View SOBS')==false){
	header('Location:home.php');
	exit();
}


?>
<script>
$(document).ready(function() {
	$('.datepicker').datepicker();
	$("#filter_sob_keywords").autocomplete("keyword_suggestions.php");
});

function discussion_sob(sob_id){
	var height = $(window).height();
	var url = "discussion_sob.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob_id);
}

function insert_discussion_note(){
	var flag = 0;
	if(document.getElementById('discussion_note').value==""){
		document.getElementById('discussion_note').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('discussion_note').style.borderColor="";
	}
	
	
	
	if(flag==0){
		var vals = $('#reply_form').serialize();
		$.post('insert_discussion_note.php', vals, function(response){
			$('#reply_table').append(response);
			document.getElementById('discussion_note').value=""
		});
	}
}

function sorting_field_name(field)
{
	var curr_sort = $('#sorting_field_name').val();
	var sort_by = $('#sorting_by').val();

	if(curr_sort == field){
		if(sort_by=='ASC')
	$('#sorting_by').val('DESC');
	else
	$('#sorting_by').val('ASC');	
	}
	
	else{
	$('#sorting_field_name').val(field);
	$('#sorting_by').val('ASC');
	}
	search_sob();
	//filter_sob();
	
}
function filter_sob()
{

	
	var sorting_field_name = $('#sorting_field_name').val();
	var sorting_by = $('#sorting_by').val();
	
	$('#page_contents').load('list_sobs.php','sorting_field_name='+sorting_field_name+'&sorting_by='+sorting_by);

}
</script>
<div id="wrapper">
    <div id="wrapper_content">
    <h1 class="page_title">Manage SOBs</h1>
<br />
<br />

      <form id="sob_filter_form" name="sob_filter_form" >
        <input type="hidden" name="sorting_field_name" id="sorting_field_name" value="s.sob_id" />
        <input type="hidden" name="sorting_by" id="sorting_by" value="ASC" />
        <div class="sob_filter_holder highlight_color">
        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td class="sub_headings" align="left">Filter SOBs</td>
            <td align="right" width="100"><a href="javascript:;" class="close_button" onclick="toggle_graph('sob_filter',this)">- Hide</a></td>
        </tr>
        </table>
        <div  id="sob_filter">
        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td width="6%"><strong>Levels</strong></td>
            <td colspan="2"> <?php
					$levels_obj = $db->query("select * from `levels` where 1");
					$levels = $levels_obj->rows;
					$l=0;
					foreach($levels as $level){						
						?>
						<input type="checkbox" id="level_<?php echo $level['level_id'];?>" name="filter_levels[<?php echo $l++;?>]" value="<?php echo $level['level_id'];?>"> <?php echo $level['level'];?> 
						<?php
					}
		?></td>
         </tr>
         <tr>
            <td><strong>Topics</strong></td>
            <td colspan="2">
            
             <?php
				$topics_obj = $db->query("select * from `topics` where 1");
				$topics = $topics_obj->rows;
				$t=0;
				foreach($topics as $topic){
					?>
					<input type="checkbox" id="topic_<?php echo $topic['topic_id'];?>" name="filter_topics[<?php echo $t++;?>]" value="<?php echo $topic['topic_id'];?>"> <?php echo $topic['topic'];?> 
					<?php
				}
		?>
            </td>
          </tr>
          <tr>
            <td><strong>SOB</strong></td>
            <td colspan="2">
           <textarea id="sob_filter" name="sob_filter" placeholder="Enter part of the SOB" style="height:17px; width:98%;"></textarea>
            </td>
          </tr>
          <tr>
            <td><strong>Start Dates</strong></td>
            <td colspan="2">
                <input type="text" id="start_date_from" name="start_date_from" class="datepicker" placeholder="Start From" style="width:80px;" />&nbsp;&nbsp;
                <input type="text" id="start_date_to" name="start_date_to" class="datepicker" placeholder="Start To" style="width:80px;" />
            </td>
          </tr>
          
          <tr>
            <td><strong>Expected Completion Dates</strong></td>
            <td colspan="2">
                
                <input type="text" id="completed_date_from" name="completed_date_from" class="datepicker" placeholder="Expected From " style="width:90px;" />&nbsp;&nbsp;
                <input type="text" id="completed_date_to" name="completed_date_to" class="datepicker" placeholder="Expected To" style="width:80px;" />
            </td>
          </tr>
          
          <tr>
            <td style="border-bottom:none;"><strong>Keywords</strong></td>
            <td><input type="text" id="filter_sob_keywords" name="filter_sob_keywords"  onkeyup="add_keyword_filter(this,event)" style="width:496px;" /></td>
            <td width="150"></td>
          </tr>
          <tr>
            <td style="border-top:none; border-right:none"><span style="border-top:none"><a class="small themebutton button" onclick="search_sob()" href="javascript:;">Filter</a></span></td>
            <td colspan="2" style="border-top:none">
            <input type="hidden" name="filter_keyword_count" id="filter_keyword_count" value="0">
               <ul id="search_sob_keywords_tags" style="list-style: none outside none; margin-left: 0; margin-top: -12px; padding: 0; width: 95%;">
               
               </ul></td>
        </tr>
        </table>
        </div>
        </div>
        <br />

        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td class="sub_headings" align="left">List of SOBs</td>
            <td align="right">
			<?php
            if(has_capabilities($uid, 'Manage SOBs')==true){
				?>
				<a class="small themebutton button" style="float:right;" onClick="add_new_sob()" href="javascript:;">Add SOB</a>
				<?php
            }
            ?>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        </table>
        </form>


        <div id="page_contents">
            <?php
			$sorting_field_name ='s.sob_id';
			$sorting_by = 'ASC';
            include 'list_sobs.php';
            ?>
      </div>
      
  </div>
</div>
<?php
include 'footer.php';
?>