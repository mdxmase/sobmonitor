<?php
session_start();
include 'login_checker.php';
include 'header.php';


if(has_capabilities($uid, 'Manage Staffs')==false){
	header('Location:home.php');
	exit();
}


?>

<div id="wrapper">
    <div id="wrapper_content">
    	<h1 class="page_title">Settings</h1>
        <div id="page_contents">
			
      	</div>
  </div>
</div>
<?php

include 'footer.php';
?>