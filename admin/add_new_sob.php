<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);
//sob_observations


?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">ADD SOB</th>
  </tr>
</table>
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<form name="sob_form" id="sob_form" enctype="multipart/form-data" action="sob_submit.php" method="POST" target="hidden_iframe">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td><strong>SOB</strong></td>
<td><textarea id="sob" name="sob" placeholder="Enter SOB" style="height:27px; width:98%;"></textarea></td>
</tr>
  <tr>
    <td width="100"><strong>URL</strong></td>
    <td><input type="text" id="url" name="url" style="width:98%;" /></td>
  </tr>
  <tr>
    <td width="100"><strong>Notes</strong></td>
    <td><input type="text" id="notes" name="notes" style="width:98%;" /></td>
  </tr>
<tr>
    <td width="100"><strong>Level / Topic</strong></td>
    <td><select id="level_id" name="level_id" style="width:250px;">
    	<option value="">Select Level</option>
        <?php
		$levels_obj = $db->query("select * from `levels` where 1");
		$levels = $levels_obj->rows;
		
		foreach($levels as $level){
			?><option value="<?php echo $level['level_id'];?>"><?php echo $level['level'];?></option><?php
		}
		?>
    </select>&nbsp;&nbsp;<select id="topic_id" name="topic_id" style="width:250px;">
    	<option value="">Select Topic</option>
       <?php
		$topics_obj = $db->query("select * from `topics` where 1");
		$topics = $topics_obj->rows;
		
		foreach($topics as $topic){
			?><option value="<?php echo $topic['topic_id'];?>"><?php echo $topic['topic'];?></option><?php
		}
		?>
    </select></td>
  </tr>
 <tr>
    <td width="100"><strong>Dates</strong></td>
    <td><input type="text" id="expected_start_date" style="width:237px;" name="expected_start_date" class="datepicker" placeholder="Start Date" /> &nbsp;&nbsp;<input type="text" id="expected_completion_date" style="width:237px;" name="expected_completion_date" class="datepicker" placeholder="Expected Completion" /></td>
  </tr>
  <tr>
    <td width="100"><strong>Keywords</strong></td>
    <td><input type="text" id="sob_keywords" name="sob_keywords" onkeyup="add_keyword(this,event)" style="width:496px;" placeholder="Please press Return key after entering each keyword" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td height="50">
    <input type="hidden" name="keyword_count" id="keyword_count" value="0">
    <ul id="sob_keywords_tags" style="list-style: none outside none; margin-left: 0; margin-top: -12px; padding: 0; width: 95%;">
    
    </ul>
    
    </td>
  </tr>
</table>
<a class="small themebutton button" style="float:right;" onClick="add_sob()" href="javascript:;">Add SOB</a>

</form>
</div>