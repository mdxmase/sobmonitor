Having trouble signing in? <br />
Please contact CHANGEME (<a href="mailto:CHANGEME@EXAMPLE.COM" target="_blank">CHANGEME@EXAMPLE.COM</a>) <a href="http://mase.cs.mdx.ac.uk/" target="_blank">http://mase.cs.mdx.ac.uk/</a>
<?php 
include_once "browser_detection.php";
$detected_array = browser_detection('full_assoc');
if($detected_array['browser_working']=="ie"){ ?>
We detected a <strong>NON-COMPATIBLE browser</strong><br /><br />
We strongly recommend 
<a href="http://www.mozilla.org/en-US/firefox/all/" target="_blank">Mozilla firefox </a> | 
<a href="https://www.google.com/intl/en/chrome/browser/" target="_blank">Chrome</a> | 
<a href="http://support.apple.com/kb/dl1531" target="_blank">Safari</a><br /><br />
Please switch to any one of the above browsers to enjoy optimal performance
<?php
}
?>
