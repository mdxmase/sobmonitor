<?php
session_start();
include 'login_checker.php';
include 'header.php';
?>
<style>
h1, h2, h3 {
padding-left:35px;	
}


#graph_legend{
	float:right;
	font-weight:bold;
}

#graph_legend ul{
	list-style: none outside none;
}

#graph_legend ul li{
float:left;
padding:5px 5px;
line-height:15px;
margin-right:10px;	
}

#graph_legend div{
float:left;

}
.legend_key{
width:15px;
height:15px;
margin-right:5px;	
}

.ideal{
background-color: #000000;	
}

.student_selected{
background-color: #008800;		
}

.other_students{
background-color: #EECA06;		
}

.tutor_detail{
	border: 1px solid #cccccc;
    clear: both;
    float: left;
    margin-left: 10px;
    padding: 20px;
    width: auto;
}

.tutor_detail h1{
	margin:0px;
	padding:0px;
	margin-bottom:5px;	
}

.tutor_detail h3{
	margin:0px;
	padding:0px;
	margin-bottom:5px;	
}

.tutor_detail h4{
	margin:0px;
	padding:0px;	
}
</style>
<link type="text/css" rel="stylesheet" href="../plugins/css/jquery.pagewalkthrough.css" />
<!--
WALKTHROUGH DISABLED 
<script type="text/javascript" src="walkthrough_scripts/dashboard_walkthrough.js"></script>-->
<script>
function sob_notes(sob_id){
	var height = $(window).height();
	var url = "sob_notes.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob_id);
}

function show_sob_list(type){
	
	
	$('.stat-boxes2 li').css('background','');
	$('#box_'+type).css('background','#F6F6F6');
	
	if(type=="1"){
		$('#content').html('Please wait... Loading...').load('list_sobs.php');
	}
	else if(type=="2"){
		$('#content').html('Please wait... Loading...').load('list_sobs.php?sob_status=1');
	}
	else if(type=="3"){
		$('#content').html('Please wait... Loading...').load('list_sobs.php?sob_status=1&levels=1');
	}
	else if(type=="4"){
		$('#content').html('Please wait... Loading...').load('list_sobs.php?sob_status=1&levels=2');
	}
	else if(type=="5"){
		$('#content').html('Please wait... Loading...').load('list_sobs.php?sob_status=1&levels=3');
	}
	else if(type=="6"){
		var sob_ids = $('#expected_sob_id').val();
		$('#content').html('Please wait... Loading...').load('list_sobs.php?sob_ids='+sob_ids);
	}
	else if(type=="7"){
		var sob_ids = $('#overdue_sob_id').val();
		$('#content').html('Please wait... Loading...').load('list_sobs.php?sob_ids='+sob_ids);
	}
	else if(type=="8"){
		$('#content').html('Please wait... Loading...').load('list_kits.php');
	}
	else if(type=="9"){
		$('#content').html('Please wait... Loading...').load('list_overdue_kits.php');
	}
	
}
</script>

<link href="css/dashboard.css" rel="stylesheet" type="text/css" /> 
<br />
<br />
<div id="wrapper">
    <div id="wrapper_content">
     
     	<?php
		
		$seven_days_from_now = date('d.m.Y',strtotime("$today_uk_format +7 days"));
		
		$sob_obj = $db->query("SELECT * FROM `sobs` WHERE 1");
		$sob_rows = $sob_obj->rows;
		
		$total_sob = 0;
		$total_observed = 0;
		$observed_1 = 0; // Threshold
		$observed_2 = 0; // Typical
		$observed_3 = 0; // Excellent
		$expected = 0;
		$over_due = 0;
		
		$expected_sob_id = "0";
		$overdue_sob_id = "0";
		
		foreach($sob_rows as $sob_row){
			$total_sob++;
			$sob_id = $sob_row['sob_id'];
			$expected_date = $sob_row['expected_completion_date'];
			$obs_obj = $db->query("SELECT * FROM `sob_observations` WHERE `student_id` = '$uid' AND `sob_id` = '$sob_id'");
			$obs_no = $obs_obj->num_rows;
			
			if($obs_no>0){
				$total_observed++;
				${'observed_'.$sob_row['level_id']}++;
			}
			else if($obs_no==0){
				if(strtotime($today_uk_format)>strtotime($expected_date)){
				
					if($sob_row['level_id']=="1"){
						$over_due++;
						$overdue_sob_id.=','.$sob_id;
					}
					
				}
			}
			
			if(strtotime($seven_days_from_now)>strtotime($expected_date) && $obs_no==0){
				if($sob_row['level_id']=="1"){
					$expected++;
					$expected_sob_id.=','.$sob_id;
				}
				
			}
			
		}

        $kits_on_loan = 0;
        $kits_overdue = 0;

        $kits_on_loan_obj= $db->query("SELECT COUNT(*) as 'count' FROM kit_loans WHERE kit_loans.student_id = '$uid' AND kit_loans.kit_returned = 0");
        $kits_on_loan = $kits_on_loan_obj->row['count'];

        $kits_overdue_obj= $db->query("SELECT COUNT(*) as 'count' FROM kit_loans WHERE kit_loans.student_id = '$uid' AND kit_loans.kit_returned = 0 AND kit_loans.kit_due_at < NOW()");
        $kits_overdue = $kits_overdue_obj->row['count'];

        ?>
     <ul class="stat-boxes2" id="dashboard_links">
         <li id="box_1"><a href="javascript:;" onclick="show_sob_list(1)"><div class="right"> <strong><?php echo $total_sob;?></strong> Total SOBS</div></a></li>
         <div id="list_of_observed_sobs" style="float:left">
         <li id="box_2"><a href="javascript:;" onclick="show_sob_list(2)"><div class="right"> <strong><?php echo $total_observed;?></strong> Observed SOBS</div></a></li>   
         <li id="box_3"><a href="javascript:;" onclick="show_sob_list(3)"><div class="right"> <strong><?php echo $observed_1;?></strong> Observed Threshold</div></a></li>   
         <li id="box_4"><a href="javascript:;" onclick="show_sob_list(4)"><div class="right"> <strong><?php echo $observed_2;?></strong> Observed Typical</div></a></li>   
         <li id="box_5"><a href="javascript:;" onclick="show_sob_list(5)"><div class="right"> <strong><?php echo $observed_3;?></strong> Observed Excellent</div></a></li> 
         </div>
        
         <li id="box_6"><a href="javascript:;" onclick="show_sob_list(6)"><div class="right"> <strong><?php echo $expected;?></strong> Expected Threshold (by <?php echo $seven_days_from_now;?>)</div></a></li>   
         <li id="box_7"><a href="javascript:;" onclick="show_sob_list(7)"><div class="right"> <strong><?php echo $over_due;?></strong> Over due Threshold</div></a></li> 
         <li id="box_8"><a href="javascript:;" onclick="show_sob_list(8)"><div class="right"> <strong><?php echo $kits_on_loan;?></strong> Kits on Loan</div></a></li>
         <li id="box_9"><a href="javascript:;" onclick="show_sob_list(9)"><div class="right"> <strong><?php echo $kits_overdue;?></strong> Overdue Kits</div></a></li>

     </ul>   
     
     <?php
	 $student_obj = $db->query("SELECT * FROM `students` WHERE `student_id` = '$uid'");
	 $student_detail = $student_obj->row;
	 if($student_detail['staff_id']!="0"){
		 $staff_id = $student_detail['staff_id'];
		 $staff_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staff_id'");
	 	 $staff_detail = $staff_obj->row;
		 ?>
         <div class="tutor_detail">
         	<h1>Tutor</h1>
            <h3><?php echo $staff_detail['firstname'];?> <?php echo $staff_detail['lastname'];?></h3>
            <h4><?php echo $staff_detail['email'];?></h4>
         </div>
     	<?php
	 }
	 ?>
     
        <input name="expected_sob_id" id="expected_sob_id" type="hidden" value="<?php echo $expected_sob_id;?>" />
        <input name="overdue_sob_id" id="overdue_sob_id" type="hidden" value="<?php echo $overdue_sob_id;?>" />
        <div style="clear:both"></div>
        
        <div id="content"></div>
        
    </div>
    

</div>
<div id="walkthrough">
	<div id="intro" style="display:none;">
		<p class="tooltipTitle">INTRODUCTION TEXT</p>
        <p>more descriptive text</p>
        <br>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
	
    <div id="STEP1" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="STEP2" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="STEP3" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="STEP4" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="done-walkthrough" style="display:none;">
		<p class="tooltipTitle">That's it!</p>
        <p>Feel free to contact us if you have additional queries.</p>
        <p>Click 'close' at the top right corner to close this walkthrough</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
	</div>
</div>



<?php
include 'footer.php';
?>
