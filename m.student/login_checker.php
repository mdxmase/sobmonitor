<?php
include_once('config.php');
include_once('db_class.php');

$registry = new Registry();
$session = new Session_Class();
$db = new MySQL_Class(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

function timeDiff($firstTime,$lastTime)
{
	$firstTime=strtotime($firstTime);
	$lastTime=strtotime($lastTime);
	$timeDiff=$lastTime-$firstTime;
	return $timeDiff;
}

if(isset($_SESSION['student_loggedin_id'])){
	$current_session 				= session_id();
	$loggedin_id 					= $_SESSION['student_loggedin_id'];
	$loggedin_id_level 				= $_SESSION['loggedin_accesslevel'];
	$loggedin_ip 					= $_SERVER['REMOTE_ADDR'];
	
	$loggedin_id 					= $db->escape($loggedin_id);
	$loggedin_id_level 				= $db->escape($loggedin_id_level);
	$loggedin_ip 					= $db->escape($loggedin_ip);
	
	$session_chk = $db->query("SELECT * FROM `validate_sessions` WHERE `session_id` = '$current_session' AND `user_logged_enc` = '$loggedin_id' AND `user_type` = '$loggedin_id_level' AND `user_ip` = '$loggedin_ip'");
	$session_exist = $session_chk->num_rows;
	
	if($session_exist>0){
		$session_row 				= $session_chk->row;
		$rowID 						= $session_row['log_id'];
		$lastActivity 				= $session_row['last_activity'];
		$session_datetime 			= date('Y-m-d H:i:s');
		$time_since_last_activity 	= timeDiff($lastActivity, $session_datetime);
		
		if($time_since_last_activity<$timeout_duration){
			$db->query("UPDATE `validate_sessions` SET `last_activity` = '$session_datetime' WHERE `log_id` = '$rowID'");
			
			$person_loggedin 		= $session_row['user_name'];
			$person_loggedin_email 	= $session_row['user_email'];
			$uid 					= $session_row['user_logged_id'];
			$uid 					= $db->escape($uid);
		}
		else{
			//SESSION TIMED OUT
			session_destroy();
			echo '<script>window.top.window.window.location="index.php";</script>';
			exit();
		}
	}
	else{
		//INVALID SESSION
		session_destroy();
		echo '<script>window.top.window.window.location="index.php";</script>';
		exit();
	}
	
}
else{
	//SESSION DOES NOT EXIST
	session_destroy();
	echo '<script>window.top.window.window.location="index.php";</script>';
	exit();
}

?>