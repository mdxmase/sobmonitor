<?php
session_start();
$path =  'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

include "../browser_detection.php";
$detected_array = browser_detection('full_assoc');

if(!isset($_SESSION['student_loggedin_id']))
{
session_destroy();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOBs</title>
<link href="css/theme.css" rel="stylesheet" type="text/css" />
<link href="css/css-buttons.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet" type="text/css" />
<script>
function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode ==13)
	login_fn();
	return true;
}
</script>
<script type="text/javascript" language="JavaScript" src="js/jquery.js"></script>
<script type="text/javascript" language="JavaScript" src="js/user_js.js"></script>
</head>

<body><br /><br /><br /><br /><br /><br />

<div id="login-box">
  <div style="margin-bottom:20px"><img src="images/index_logo.png" /></div>
<form method="post">
    <h1> STUDENT LOGIN</h1>
      <dl>
     
      <dt>
        <label for="email">IT User ID : <span class="help_text">(Normally 2 letters and numbers, e.g. AB1234)</span></label></dt>
      <dd><input type="text" tabindex="1" name="username" id="username" autofocus="autofocus" onkeypress="return isNumberKey(event)"></dd>

      <dt><label for="password">Password: <span class="help_text">(This is the password you use to log-in to unihub)</span></label></dt>
      <dd><input type="password" tabindex="2" name="password" id="password" onkeypress="return isNumberKey(event)"></dd>
    </dl>
<div id="validate_div"></div>
    
    
    <a href="javascript:;" class="large gray button" id="" onclick="login_fn()" style="float:right"><span>Login</span></a>
      
</form>
<?php include "../incompatible_browser_alert.php";?>
</div>
<?php
}
else{
	?>
    <script>
	window.location="home.php";
	</script>
    <?php
}
?>
</body>
</html>