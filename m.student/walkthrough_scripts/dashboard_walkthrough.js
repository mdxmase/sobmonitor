$(document).ready(function(){

	$('#walkthrough').pagewalkthrough({

		steps:
        [
               {
                   wrapper: '',
                   margin: 0,
                   popup:
                   {
                       content: '#intro',
                       type: 'modal',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }        
               },
			   {
                   wrapper: '#box_1',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP1',
                       type: 'tooltip',
                       position: 'right',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
			   
			   {
                   wrapper: '#list_of_observed_sobs',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP2',
                       type: 'tooltip',
                       position: 'bottom',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
			   			   
			  {
                   wrapper: '#box_6',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP3',
                       type: 'tooltip',
                       position: 'left',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
			   
			   {
                   wrapper: '#box_7',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP4',
                       type: 'tooltip',
                       position: 'left',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
               
               
               {
                   wrapper: '',
                   margin: '0',
                   popup:
                   {
                       content: '#done-walkthrough',
                       type: 'modal',
                       position: '',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }             
               }

        ],
        name: 'Walkthrough',
        onLoad: true,
        onClose: function(){
            
            return true;
        },
        onCookieLoad: function(){
            
			//alert('This callback executed when onLoad cookie is FALSE');
            return true;
        }

	});






  $('.prev-step').live('click', function(e){
      $.pagewalkthrough('prev',e);
  });

  $('.next-step').live('click', function(e){
      $.pagewalkthrough('next',e);
  });

  $('.restart-step').live('click', function(e){
      $.pagewalkthrough('restart',e);
  });

  $('.close-step').live('click', function(e){
      $.pagewalkthrough('close');
  });

});