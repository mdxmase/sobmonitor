$(document).ready(function(){

	$('#walkthrough').pagewalkthrough({

		steps:
        [
               {
                   wrapper: '',
                   margin: 0,
                   popup:
                   {
                       content: '#intro',
                       type: 'modal',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }        
               },
			   {
                   wrapper: '#progress_bargraphs_holder',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP1',
                       type: 'tooltip',
                       position: 'right',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
			   
			   {
                   wrapper: '#threshold_bargraphs_holder',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP2',
                       type: 'tooltip',
                       position: 'top',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
			   			   
			  {
                   wrapper: '#typical_bargraphs_holder',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP3',
                       type: 'tooltip',
                       position: 'top',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
			   
			   {
                   wrapper: '#excellent_bargraphs_holder',
                   margin: '0',
                   popup:
                   {
                       content: '#STEP4',
                       type: 'tooltip',
                       position: 'top',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '250'
                   }     
               },
               
               
               {
                   wrapper: '',
                   margin: '0',
                   popup:
                   {
                       content: '#done-walkthrough',
                       type: 'modal',
                       position: '',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }             
               }

        ],
        name: 'Walkthrough',
        onLoad: true,
		scrollSpeed: 3000,
        onClose: function(){
            
            return true;
        },
        onCookieLoad: function(){
            
			//alert('This callback executed when onLoad cookie is FALSE');
            return true;
        }

	});






  $('.prev-step').live('click', function(e){
      $.pagewalkthrough('prev',e);
  });

  $('.next-step').live('click', function(e){
      $.pagewalkthrough('next',e);
  });

  $('.restart-step').live('click', function(e){
      $.pagewalkthrough('restart',e);
  });

  $('.close-step').live('click', function(e){
      $.pagewalkthrough('close');
  });

});