function login_fn()
{
	var name=document.getElementById("username").value;
	var pwd=document.getElementById("password").value;
	
	if(name !="" && pwd!=""){
	$.post('login_page.php',{user:name, pass:pwd},function(response){
		$('#validate_div').html(response);
	})
	}
	else{
		$('#validate_div').html('<span style="color:red">Please enter valid User ID and Password</span>');
	}

}


function logout()
{
	document.getElementById('menu_div').innerHTML="";
	ajaxpage("login.php",'center_div');
}
function strcmp(a, b) {
    if (a.toString() < b.toString()) return -1;
    if (a.toString() > b.toString()) return 1;
    return 0;
}


function submit_fn()
{
	var email=document.getElementById("email").value;
	
	if(email =='')
	{
		document.getElementById("email").style.borderColor = "red";
		$.jGrowl("Please enter a valid email address or username");
	}
	else
	{
		document.getElementById('email').style.borderColor="#CCCCCC";
		
		if(email != '')
		{
			if(echeck_file($('#email').val()))
			{
				document.getElementById('email').style.borderColor="#CCCCCC";
				document.forgot_pwd_form.action="check_user_info.php";
				document.forgot_pwd_form.submit();
				
			}
			else
			{
				document.getElementById('email').style.borderColor="red";
				$.jGrowl("Please enter a valid email address");
			}
		}
		
		document.forgot_pwd_form.action="check_user_info.php";
		document.forgot_pwd_form.submit();
	}
}

function reset_pwd()
{
	var pwd=document.getElementById("newpwd").value;
	var confirm_pwd=document.getElementById("confirm_newpwd").value;
	var $flag = 0;
	
	if(pwd == '')
	{
		document.getElementById("newpwd").style.borderColor = "red";
		$flag = 1;
	}
	if(confirm_pwd == '')
	{
		document.getElementById("confirm_newpwd").style.borderColor = "red";
		$flag = 1;
	}
	
	if($flag == 0 )
	{
		document.getElementById("newpwd").style.borderColor = "#CCCCCC";
		document.getElementById("confirm_newpwd").style.borderColor = "#CCCCCC";
		
		if(strcmp(pwd,confirm_pwd) != 0)
		{
			$.jGrowl("Passwords do not match");		
		}
		else
		{
			document.reset_pwd_form.action="update_new_password.php";
			document.reset_pwd_form.submit();
		}
	}
}

function echeck_file(str,which) {	// here str is d entered email value

	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)
	if (str.indexOf(at)==-1){
	   return false
	}

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
	   return false
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		return false
	}

	 if (str.indexOf(at,(lat+1))!=-1){
		return false
	 }

	 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		return false
	 }

	 if (str.indexOf(dot,(lat+2))==-1){
		return false
	 }
	
	 if (str.indexOf(" ")!=-1){
		return false
	 }

	 return true;					
}


function ajaxpage_login(url, containerid){
var page_request = false
if (window.XMLHttpRequest) // if Mozilla, Safari etc
page_request = new XMLHttpRequest()
else if (window.ActiveXObject){ // if IE
try {
page_request = new ActiveXObject("Msxml2.XMLHTTP")
}
catch (e){
try{
page_request = new ActiveXObject("Microsoft.XMLHTTP")
}
catch (e){}
}
}
else
return false
page_request.onreadystatechange=function(){
loadpage_login(page_request, containerid,url)
}
page_request.open('GET', url, true)
page_request.send(null)
}

function loadpage_login(page_request, containerid,url)
{
   
if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1))
{
    document.getElementById(containerid).innerHTML=page_request.responseText;
    if(document.getElementById('status').value=="0")
    {
        document.getElementById("username").value="";
        document.getElementById("password").value="";
        document.getElementById(containerid).innerHTML="<font color='red'>Login Failed...</font>";
    }
    else
    {
        window.location="home.php";
    }
   
}

if(page_request.readyState == 1 || page_request.readyState == 2 || page_request.readyState == 3 )
document.getElementById(containerid).innerHTML="<img src='images/ajax-loader.gif'/><span>Validating ...</span>";
}