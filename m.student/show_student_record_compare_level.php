<?php
session_start();
include_once('login_checker.php');
$today = date("Y-m-d");



$level = $_GET['level'];

if(!isset($_GET['inactive'])){
	$only_active = 	' AND `student_status` = 0';
}
else{
	$only_active = '';
}

$stud_obj = $db->query("SELECT * FROM `students` WHERE 1 $only_active");
$students = $stud_obj->rows;


$values = array();
$labels = array();
$colors = array();

	if($_GET['topics']!=""){
		$topics = $_GET['topics'];
		$add_filter_topics = " AND topic_id IN ($topics) ";
		
		$flag = 1;
			
			$topics_obj = $db->query("select * from `topics` where topic_id IN ($topics)");
			$topics = $topics_obj->rows;
			$topics_txt = "";
			foreach($topics as $topic){
				if($topics_txt=="")
					$topics_txt= $topic['topic'];
				else
					$topics_txt.=', '.$topic['topic'];
				
			}
			
			$filter_topics = $topics_txt;
	}
	else{
		$add_filter_topics = "";
		$filter_topics = 'All';
	}
	
	if($_GET['from_date']!=""){
		$from_date = date_mysql($_GET['from_date']);
		$add_filter_from =" AND expected_completion_date >= '$from_date' ";
	}
	else{
		$add_filter_from = "";	
	}
	
	if($_GET['to_date']!=""){
		$to_date = date_mysql($_GET['to_date']);
		$add_filter_to =" AND expected_completion_date <= '$to_date' ";
	}
	else{
		$add_filter_to = "";
	}
	
		if(isset($_GET['from_date']) && $_GET['from_date']!=""){
			$filter_date = 'Between '.$_GET['from_date'] . ' and '.$_GET['to_date'];
		}
		else{
			$filter_date =  'Any';
		}
if ($level != "ALL") {
    $net_total_obj = $db->query("SELECT COUNT(*) AS net_total FROM `sobs` WHERE `level_id` = '$level' $add_filter_topics $add_filter_from $add_filter_to and expected_completion_date<'$today'");
} else {
    $net_total_obj = $db->query("SELECT COUNT(*) AS net_total FROM `sobs` WHERE 1 $add_filter_topics $add_filter_from $add_filter_to and expected_completion_date<'$today'");
}
	$net_total = $net_total_obj->row;
	$ideal_mark = $net_total['net_total'];
	array_push($values,$ideal_mark);
	array_push($labels," ");
	array_push($colors,"#000000");


if ($level != "ALL") {
    $max_no_of_sobs = $db->query("SELECT COUNT(*) AS max_no_of_sobs FROM `sobs` WHERE `level_id` = '$level' $add_filter_topics $add_filter_from $add_filter_to");
} else {
    $max_no_of_sobs = $db->query("SELECT COUNT(*) AS max_no_of_sobs FROM `sobs` WHERE 1 $add_filter_topics $add_filter_from $add_filter_to");
}
	$max_no_of_sobs_total = $max_no_of_sobs->row;
	$max_y_value = $max_no_of_sobs_total['max_no_of_sobs'];


foreach($students as $student){
	
	
	
	$stud_id = $student['student_id'];

	if ($level != "ALL") {
        $filter_query = "SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$stud_id' AND `sob_id` IN (SELECT sob_id FROM `sobs` WHERE `level_id` = '$level' $add_filter_topics $add_filter_from $add_filter_to)";
    } else {
        $filter_query = "SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$stud_id'";
    }
	$user_finished_obj = $db->query($filter_query);
	$user_finished = $user_finished_obj->row;
	$user_finish = $user_finished['user_finished_total'];

	
	if($student['student_id']==strtoupper($uid)){
		
		array_push($values,$user_finish);
		array_push($labels," ");
		array_push($colors,"#008000");
		
		$student_name = $student['firstname'] . " " . $student['lastname'];
	}
	else{
		array_push($values,$user_finish);
		array_push($labels," ");
		array_push($colors,"#EECA06");

	}
	



	
}


array_multisort($values, $labels, $colors);

$vals = "";
foreach($values as $value){
	if($vals==""){
		$vals = $value;
	}
	else{
		$vals.=",".$value;
	}
}

$labs = "";
foreach($labels as $label){
	if($labs==""){
		$labs = '" "';
	}
	else{
		$labs.=",".'" "';
	}
}


$cols = "";
foreach($colors as $color){
	if($cols==""){
		$cols = '"'.$color.'"';
	}
	else{
		$cols.=",".'"'.$color.'"';
	}
}

$applied_filter = "<strong>Topics</strong> : $filter_topics <br /><strong> Expected Completion Date</strong> : $filter_date";

$json = '{
			"values" : ['.$vals.'],
			"labels" : ['.$labs.'],
			"colors" : ['.$cols.'],
			"student" : "'.$student_name.'",
			"applied_filter"  : "'.$applied_filter.'",
			"max_y_value" : "'.$max_y_value.'"
		}';
echo $json;		
?>