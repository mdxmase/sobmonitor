<?php
session_start();
include 'login_checker.php';
include 'header.php';
?>

<link type="text/css" rel="stylesheet" href="../plugins/css/jquery.pagewalkthrough.css" />
<!--
//WALKTHROUGH DISABLED
<script type="text/javascript" src="walkthrough_scripts/reports_walkthrough.js"></script>-->

<link class="include" rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
<script src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>

<script>

function show_student_record_compare(){
	
		$('#content').css('display','')
		url = 'show_student_record_compare_level.php?level=1';
		
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, '1', data.applied_filter,data.max_y_value)
		});
		
		var url = 'show_student_record_compare_level.php?level=2';
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, '2', data.applied_filter,data.max_y_value)
		});
		
		var url = 'show_student_record_compare_level.php?level=3';
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, '3', data.applied_filter,data.max_y_value)
		});

        var url = 'show_student_record_compare_level.php?level=ALL';
        $.getJSON(url, function(data){
            generate_graph(data.values, data.labels, data.colors, '4', data.applied_filter,data.max_y_value)
        });
}

function generate_graph(values, labels, colors, holder, applied_filter,max_y_value){
	$.jqplot.config.enablePlugins = true;
			var s1 = values;
			var ticks = labels;

			plot1 = $.jqplot('chart'+holder, [s1], {
				// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
				animate: !$.jqplot.use_excanvas,
				seriesColors:colors,
				
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					shadow : false,
					rendererOptions: {
					// Set varyBarColor to tru to use the custom colors on the bars.
					varyBarColor: true
				},
					pointLabels: { show: false }
				},
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks,
						label : 'Progress of students + Expected progress (by <?php echo date('d.m.Y');?>)',
						labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					},
					yaxis:{
						tickOptions:{
						formatString: "%d",
						fontSize: '12px'
						},
					   label:'Number of SOBs',
					    min:0,
  		            	max: max_y_value,
					   labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					   labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					}
				},
				highlighter: { show: true }
			});
			plot1.replot();
			$('#graph_filter_info_'+holder).html(applied_filter);
}

$(document).ready(function() {
	show_student_record_compare()
    $('#attendanceInfo').html('Loading... Please wait...').load('search_attendance.php');
});


</script>
<div id="wrapper">
    <div id="wrapper_content">
		<h1 class="page_title">Reports</h1>
        <div id="content">
    	
        
<?php
$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_id` = '$uid'");
$stud_no = $stud_obj->num_rows;
extract($_GET);
if($stud_no==0){
	
	?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1">
		 <tr>
			<Td align="center"><b style="color:#F00">Invalid Student ID</b></Td>
		  </tr>
		</table>
    <?php
	
}
else{
	$stud_details = $stud_obj->row;
	$student_id = $stud_details['student_id'];
	
	?>
			<table width="100%" cellpadding="0" cellspacing="0">
	
				<tr>
					<td class="sub_headings" align="left"><span><?php echo $stud_details['firstname'];?> <?php echo $stud_details['lastname'];?> (<?php echo $stud_details['student_number'];?>)</span> </td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				</tr>
	
			</table>
		<div style="width:600px;" id="progress_bargraphs_holder">
		<?php
		$today = date('Y-m-d');
		$level_obj = $db->query("select DISTINCT l.level_id, l.level from `sobs` s, `levels` l where s.level_id = l.level_id ORDER BY l.level_id");
		$levels = $level_obj->rows;
		
		foreach($levels as $level){
			$level_id = $level['level_id'];
			$total_obj = $db->query("SELECT COUNT(*) AS total FROM `sobs` s, `levels` l WHERE s.level_id = l.level_id AND l.level_id = '$level_id'");
			$tot = $total_obj->row;
			
			$user_tot_obj = $db->query("SELECT COUNT(*) AS user_total FROM `sobs` s, `levels` l WHERE s.level_id = l.level_id AND l.level_id = '$level_id' AND s.expected_completion_date < '$today'");
			$user_tot = $user_tot_obj->row;
			
			$user_finished_obj = $db->query("SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` IN (SELECT sob_id FROM `sobs` WHERE `level_id` = '$level_id') AND (observed_on <= '$today' AND observed_on != '0000-00-00')");
			$user_finished = $user_finished_obj->row;
			
			$total = $tot['total']; 
			$user_total = $user_tot['user_total']; 
			$user_finish = $user_finished['user_finished_total'];
			
			$achieved = ($user_finish/$total)*100;
			$achievement = ($user_total/$total)*100;
			?>
            <b class="level_display"><?php echo $level['level'];?></b>
            <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : ' . $total;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed : ' . $user_finish;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected : ' . $user_total;?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
            <?php
		}
        // Create Progress Bar for Overall
        $total_obj = $db->query("SELECT COUNT(*) AS total FROM `sobs`");
        $tot = $total_obj->row;

        $user_tot_obj = $db->query("SELECT COUNT(*) AS user_total FROM `sobs` s WHERE s.expected_completion_date < '$today'");
        $user_tot = $user_tot_obj->row;

        $user_finished_obj = $db->query("SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$student_id' AND (observed_on <= '$today' AND observed_on != '0000-00-00')");
        $user_finished = $user_finished_obj->row;

        $total = $tot['total'];
        $user_total = $user_tot['user_total'];
        $user_finish = $user_finished['user_finished_total'];

        $achieved = ($user_finish/$total)*100;
        $achievement = ($user_total/$total)*100;
        ?>
            <b class="level_display"><?php echo "Overall";?></b>
            <div class="process_bar">
                <div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                    <table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                        <tr>
                            <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                            <td>&nbsp;<?php echo 'Total : ' . $total;?>&nbsp;&nbsp;</td>
                            <td bgcolor="#008800" width="10">&nbsp;</td>
                            <td>&nbsp;<?php echo 'Observed : ' . $user_finish;?>&nbsp;&nbsp;</td>
                            <td bgcolor="#000000" width="1">&nbsp;</td>
                            <td>&nbsp;<?php echo 'Expected : ' . $user_total;?>&nbsp;&nbsp;</td>
                        </tr>
                    </table>

                </div>
            </div>
            <?php
}

?>
</div>

		<?php $legend_graph = '<span id="graph_legend">
                <ul>
                    <li><div class="legend_key ideal"></div>Expected</li>
                    <li><div class="legend_key student_selected"></div><div class="display_student_name">'.$stud_details['firstname'].' '. $stud_details['lastname'] .'</div></li>
                    <li><div class="legend_key other_students"></div>Other students</li>
                </ul>
            </span>'; 
			?>
            
            <div class="canvas_holder" id="threshold_bargraphs_holder">
            	<div class="graph_display_details"><h2>Threshold </h2>
                    
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart1" style=" width:100%; height:450px;"></div>
            </div>
            
            <div class="clear"></div>
            <br />
			<br />
            <div class="canvas_holder" id="typical_bargraphs_holder">
            	<div class="graph_display_details"><h2>Typical </h2>
                    
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart2" style=" width:100%; height:450px;"></div>
            </div>
            <div class="clear"></div>
            <br />
			<br />
            
            <div class="canvas_holder" id="excellent_bargraphs_holder">
            	<div class="graph_display_details"><h2>Excellent </h2>
                    
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart3" style=" width:100%; height:450px;"></div>
            </div>
            <div class="clear"></div>
            <br />
            <br />

            <div class="canvas_holder" id="overall_bargraphs_holder">
                <div class="graph_display_details"><h2>Overall</h2>

                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                <div id="chart4" style=" width:100%; height:450px;"></div>
            </div>

            <div id="attendanceInfo"></div>
            
    		
        
        </div>
        
    </div>
    	
</div>

<div id="walkthrough">
	<div id="intro" style="display:none;">
		<p class="tooltipTitle">INTRODUCTION TEXT</p>
        <p>more descriptive text</p>
        <br>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
	
    <div id="STEP1" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="STEP2" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="STEP3" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="STEP4" style="display:none;">
		<p class="tooltipTitle">TITLE</p>
        <p>more info..</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="done-walkthrough" style="display:none;">
		<p class="tooltipTitle">That's it!</p>
        <p>Feel free to contact us if you have additional queries.</p>
        <p>Click 'close' at the top right corner to close this walkthrough</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
	</div>
</div>

<?php
include 'footer.php';
?>
