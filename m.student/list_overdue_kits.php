<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');


?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
</table>
<div>
    <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
        <tr class="table_heading">
            <th align="left">Tag</th>
            <th align="left">Kit Type</th>
            <th align="left">Check Out</th>
            <th align="left">Due Date</th>

        </tr>

        <?php
        $query="SELECT kits.kit_asset_tag, kit_type.kit_type_name, kit_loans.kit_checked_out_at, kit_loans.kit_due_at FROM kit_loans LEFT OUTER JOIN students ON kit_loans.student_id = students.student_id LEFT OUTER JOIN kits ON kit_loans.kit_id = kits.kit_id LEFT OUTER JOIN kit_type ON kits.kit_type_id = kit_type.kit_type_id WHERE kit_loans.student_id = '$uid' AND kit_loans.kit_returned = 0 AND kit_loans.kit_due_at < NOW() ORDER BY kit_loans.kit_checked_out_at DESC";

        $loans_obj = $db->query($query);
        $loans_no = $loans_obj->num_rows;

        if($loans_no!=0){
            $loans = $loans_obj->rows;
            $s=0;
            foreach($loans as $loan){
                ?>
                <tr>
                    <td align="left" valign="top"><?php echo $loan['kit_asset_tag'];?></td>
                    <td align="left" valign="top"><?php echo $loan['kit_type_name'];?></td>
                    <td align="left" valign="top"><?php echo date("d-m-Y H:i", strtotime($loan['kit_checked_out_at']));?></td>
                    <td valign="top" align="left"><?php echo date("d-m-Y H:i", strtotime($loan['kit_due_at']));?></td>


                </tr>
                <?php
            }
        }
        else{
            ?>
            <tr>
                <Td align="center" colspan="7"><br /><b>-- No Overdue Loans Found --</b></Td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>