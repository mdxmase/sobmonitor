<?php
// db properties
if($_SERVER['SERVER_NAME'] == 'localhost' ){
  // This is used to test locally
date_default_timezone_set('Europe/London');


define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'CHANGEME');
define('DB_PASSWORD', 'CHANGEME');
define('DB_DATABASE', 'CHANGEME');
define('DB_PREFIX', '');
}
else{
// This is used when deployed on a server
date_default_timezone_set('Europe/London');
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'CHANGEME');
define('DB_PASSWORD', 'CHANGEME');
define('DB_DATABASE', 'CHANGEME');
define('DB_PREFIX', '');
}


$timeout_duration = 7200;
$today_uk_format = date("d.m.Y");

if(!function_exists(date_ft))
{
	function date_ft($mysql_date)
	{
		$indian_date = date('d.m.Y',strtotime("$mysql_date"));
		return $indian_date;
	}
}

if(!function_exists(num_format))
{
	function num_format($amount)
	{
		return number_format($amount,2);
	}	
}

if(!function_exists(date_mysql))
{
	function date_mysql($indian_date)
	{
		$mysql_dates = date('Y-m-d',strtotime("$indian_date"));
		return $mysql_dates;
	}
}

if(!function_exists(mysql_to_indian_date))
{
	function mysql_to_indian_date($mysql_date)
	{
		if($mysql_date=="0000-00-00")
		{
			return "";
		}
		else
		{
			$indian_date = date('d.m.Y',strtotime("$mysql_date"));
			return $indian_date;
		}
	}
}


if(!function_exists(DayDifference))
{
	function DayDifference($start, $end) {
	  $start = strtotime($start);
	  $end = strtotime($end);
	  $difference = $end - $start;
	  return round($difference / 86400);
	}
}



if(!function_exists(time_between))
{
	function time_between($from,$to,$current)
	{	
		$time = $current;
		$date = date('Y-m-d') . ' ' . $time;
		
		if(date($from)>date($to))
		{
			if(date($time)<date($to))
			{
				$from = date('Y-m-d',strtotime("-1 day")) . ' ' . $from;
				$to = date('Y-m-d') . ' ' . $to;
			}
			else
			{
				$to = date('Y-m-d',strtotime("+1 day")) . ' ' . $to;
				$from = date('Y-m-d') . ' ' . $from;
			}
			
		}
		else
		{
			$from = date('Y-m-d') . ' ' . $from;
			$to = date('Y-m-d') . ' ' . $to;
		}
		
		if($date>$from && $date<$to) {
			return "opened";
		} else {
			return 'closed';
		}	
	}
}



?>
