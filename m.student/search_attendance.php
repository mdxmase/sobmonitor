<?php
session_start();
include 'login_checker.php';

extract($_GET);

if(isset($fromweek) && $fromweek!=""){
	$from = $fromweek;
}
else{
	$from = 1;
}

if(isset($toweek) && $toweek!=""){
	$to = $toweek;
}
else{
	$to = 24;
}

$student_obj = $db->query("SELECT student_number FROM `students` WHERE student_id = $uid");
$student_number = $student_obj->row['student_number'];



?>
<h1 class="page_title">Attendance</h1>
<br>
<h1 class="page_title">Timetabled sessions</h1>
 <div style="width:100%; padding:10px 0px 5px 0px;" align="right" id="total_perc">Calculating</div>
 <table width="100%" border="0" cellspacing="1" cellpadding="10" class="content_table">
      <tr class="table_heading">
        <th width="60">CRN</th>
        <th width="230">Details</th>

        <?php
		$total_weeks=0;
		for($i=$from;$i<=$to;$i++){
			?>
            <th>W<?php echo $i;$total_weeks++;?></th>
            <?php
		}
		?>
        <th>% </th>
      </tr>
      <?php
	  $query = $db->query("SELECT c.crn, c.codetype, c.day, c.room, c.starttime, c.endtime, count(s.crn) as expected FROM `CRNlist` as c, `student_timetable` as s WHERE c.crn = s.crn AND s.student_number = '$student_number' group by c.crn");

	  $timetabled_crns = "0";	  
	  
	  if($query->num_rows>0){
	  
	  $crn_list = $query->rows;

	  foreach($crn_list as $crn){
		  $crn_no = $crn['crn'];
		  ?>
      <tr>
        <td><?php echo $crn['crn'];?></td>
        <td><?php echo $crn['codetype'];?> - <?php echo $crn['room'];?> - <?php echo $crn['day'];?> (<?php echo $crn['starttime'];?> - <?php echo $crn['endtime'];?>)</td>

        <?php
	
		$attendees_obj = $db->query("SELECT w.week_number, count(a.week) as attendees FROM `CRNlist` as c, `attendance` as a, `week` as w WHERE  c.crn = a.crn and c.crn = '$crn_no' and w.week_number = a.week and w.week_number between $from and $to AND a.studid = '$student_number' group by w.week_number  ORDER BY w.week_number ASC");
		$attendees_arr = $attendees_obj->rows;
		$attendee = array();
		
		$attended_weeks=0;
		
		foreach($attendees_arr as $attendees ){
			$the_week = $attendees['week_number'];
			$attendee[$the_week] = $attendees['attendees'];
	
		}
		$timetabled_crns.=",".$crn_no;
		
		for($j=$from;$j<=$to;$j++){
			if($attendee[$j]=="1"){
				$present_absent = 'Y';
				$attended_weeks++;
				
			}
			else{
				$present_absent = '-';
			}
			
			
			?>
            <td align="center"><?php echo $present_absent;?></td>
            <?php
		}
		?>
        <td align="center"><?php $perc = ($attended_weeks/$total_weeks)*100; echo number_format($perc,2)?>%</td>
      </tr>
      	<?php
	  }
	  }
	  else{
		 ?>
         <tr>
         	<td colspan="<?php echo $total_weeks+3;?>" align="center"><strong>No timetabled sessions found</strong></td>
         </tr>
         <?php 
	  }
	  ?>
    </table>
    <br />
    
    
    <?php
	
	$query = $db->query("SELECT w.week_id, c.crn, c.codetype, c.room, c.day, c.starttime, c.endtime FROM `CRNlist` as c, `attendance` as a, `week` as w WHERE c.crn = a.crn AND a.studid = '$student_number' AND w.week_id = a.week AND a.crn NOT IN ($timetabled_crns) AND w.week_number between $from and $to");
	
	$all_attendance = $query->rows;
	$num_rows = $query->num_rows;
	//print_r($all_attendance);
	
	if($num_rows>0){
	?>
    <h1 class="page_title">Non-timetabled sessions</h1>
    <?php
	}
	$week_head = 0;
	for($weekrange = $from;$weekrange<=$to;$weekrange++){
		
		for($i=0;$i<$num_rows;$i++){
			if($all_attendance[$i]['week_id'] == $weekrange){
			if($week_head!=$weekrange){
				$week_head=$weekrange;
				
				if($week_head!=0){
					echo '</table><br>';
				}
				?>
                <table width="300" border="0" cellspacing="1" cellpadding="10" class="content_table">
                <tr class="table_heading">
                <th colspan="2"><b>Week <?php echo $week_head ;?></b></th>
                </tr>
				<?php
			}
			?>
			 <tr>
                <td><?php echo $all_attendance[$i]['crn'] ;?></td>
                <td><?php echo $all_attendance[$i]['codetype'];?> - <?php echo $all_attendance[$i]['room'];?> - <?php echo $all_attendance[$i]['day'];?> (<?php echo $all_attendance[$i]['starttime'];?> - <?php echo $all_attendance[$i]['endtime'];?>)</td>
                </tr>
				<?php
			}
		}
		
	}
	
/*	foreach($all_attendance as $attendance){
		?>
        <tr>
        	<td><?php echo $attendance['week_id'];?></td>
            <td><?php echo $attendance['crn'];?></td>
            <td><?php echo $attendance['codetype'];?> - <?php echo $attendance['room'];?> - <?php echo $attendance['day'];?> (<?php echo $attendance['starttime'];?> - <?php echo $attendance['endtime'];?>)</td>
        </tr>
        <?php
	}*/
    ?>
    </table>
<script>
document.getElementById('total_perc').innerHTML='%  - Attendance(100% = <?php echo $total_weeks;?> Weeks)';
</script>